﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using Photon.Pun;

namespace CW
{
    public class NetworkVRTKPlayer : MonoBehaviourPunCallbacks, IPunObservable
    {
        // higher value -> faster update
        public float SmoothingDelay = 10;

        [Header("Avatar Objects")]
        public GameObject head;
        public GameObject rightController;
        public GameObject leftController;


        protected GameObject[] avatarObjects = new GameObject[3];
        protected GameObject[] VRTKObjects = new GameObject[3];

        // Networking
        protected Vector3[] networkPositions = new Vector3[3];
        protected Quaternion[] networkRotations = new Quaternion[3];


        protected virtual void Start()
        {
            // if this is the local player...
            if (photonView.IsMine)
            {
                // subscribe to when the headset changes
                VRTK_SDKManager.AttemptAddBehaviourToToggleOnLoadedSetupChange(this);
            }
        }

        public override void OnEnable()
        {
            base.OnEnable();

            // Set the avatar objects
            avatarObjects = SetAvatarObjects();

            // if this is the local player...
            if (photonView.IsMine)
            {
                // get the VRTK object transforms
                VRTKObjects = GetVRTKObjects();

                // disable the avatar objects
                EnableAvatarObjects(false);
            }
            // if this is a network player
            else
            {
                // enable the avatar objects
                EnableAvatarObjects(true);
            }
        }

        protected virtual GameObject[] GetVRTKObjects()
        {
            GameObject VRTKHeadset = VRTK_DeviceFinder.HeadsetTransform().gameObject;
            GameObject VRTKLeftController = VRTK_DeviceFinder.GetControllerLeftHand();
            GameObject VRTKRightController = VRTK_DeviceFinder.GetControllerRightHand();

            // Head - Left - Right
            return new GameObject[] { VRTKHeadset, VRTKLeftController, VRTKRightController };
        }

        protected virtual GameObject[] SetAvatarObjects()
        {
            // Head - Left - Right
            return new GameObject[] { head, leftController, rightController };
        }

        protected virtual void EnableAvatarObjects(bool enable)
        {
            for (int i = 0; i < avatarObjects.Length; i++)
            {
                avatarObjects[i].SetActive(enable);
            }
        }

        public override void OnDisable()
        {
            base.OnDisable();
            VRTKObjects = null;
        }

        protected virtual void OnDestroy()
        {
            VRTK_SDKManager.AttemptRemoveBehaviourToToggleOnLoadedSetupChange(this);
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                // THIS IS OUR LOCAL SCENE --> we need to send the information
                stream.SendNext(networkPositions);
                stream.SendNext(networkRotations);

            }
            else
            {
                // THIS IS NOT OUR LOCAL SCENE --> we need to receive the information
                networkPositions = (Vector3[])stream.ReceiveNext();
                networkRotations = (Quaternion[])stream.ReceiveNext();
            }
        }

        protected virtual void Update()
        {
            if (photonView.IsMine)
            {
                // THIS IS OUR LOCAL SCENE

                // for each object...
                for (int i = 0; i < networkPositions.Length; i++)
                {
                    // get the VRTK transform info for the object
                    Vector3 vp = VRTKObjects[i].transform.position;
                    Quaternion vr = VRTKObjects[i].transform.rotation;

                    // copy it to the network info
                    networkPositions[i] = vp;
                    networkRotations[i] = vr;
                }
            }
            else
            {
                // NOT OUR LOCAL SCENE

                // for each object...
                for (int i = 0; i < networkPositions.Length; i++)
                {
                    // move the object to the network pos and rot
                    avatarObjects[i].transform.position = Vector3.Lerp(avatarObjects[i].transform.position, networkPositions[i], Time.deltaTime * SmoothingDelay);
                    avatarObjects[i].transform.rotation = Quaternion.Lerp(avatarObjects[i].transform.rotation, networkRotations[i], Time.deltaTime * SmoothingDelay);
                }
            }
        }

    }

}