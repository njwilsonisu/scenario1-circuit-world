﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

/* -------------------------------------------------------------------------------------------------------
        Important Items:
            - We only use the right "controller", and place it at the center
            - We do not allow the user to move the controller position in any way
            - To interact with objects, the user must first "grab" the object with the laser point
                - Turn on the laser pointer -> grab the object
                    - Desktop: right click -> left click
                    - HMDs: just lightly touch the Touchpad/Joystick -> Trigger Press
            - 
 ------------------------------------------------------------------------------------------------------- */

public class SimulatorInput : SDK_InputSimulator
{
    public KeyCode touchpadTouch = KeyCode.None;
    public KeyCode triggerTouch = KeyCode.None;

    [Tooltip("The position of the controller relative to the Neck")]
    public Vector3 controllerPosition = new Vector3(0f, -0.15f, 0.5f);

    protected virtual void SetVariables()
    {
        // Unused
        
        leftHandColor = Color.clear;
        mouseMovementKey = KeyCode.None;
        changeHands = KeyCode.None;
        handsOnOff = KeyCode.None;
        rotationPosition = KeyCode.None;
        changeAxis = KeyCode.None;

        //purposefully obscure so the user does not touch them
        touchModifier = KeyCode.PageUp;
        hairTouchModifier = KeyCode.PageDown;

        // ensure all the other variables are corrected
        showControlHints = true;
        lockMouseToView = true;
        mouseMovementInput = MouseInputMode.Always;
        rightHandColor = Color.cyan;
        distancePickupModifier = KeyCode.LeftControl;
        distancePickupRight = KeyCode.Mouse0;
        distancePickupLeft = KeyCode.None;


        // Actual controls
        
        // Enable laser
        touchpadAlias = KeyCode.Space;
        // Grab
        gripAlias = KeyCode.F;
        // Use/Select
        triggerAlias = KeyCode.Mouse0;

        //Open Object Menu
        buttonOneAlias = KeyCode.Mouse1;

        // Toggle Control Hints
        toggleControlHints = KeyCode.F1;

        // Toggle Menu
        toggleMouseLock = KeyCode.Escape;

        // No Use Yet
        buttonTwoAlias = KeyCode.F1;
        startMenuAlias = KeyCode.None;

        touchpadTouch = KeyCode.None;
        triggerTouch = KeyCode.None;


        controllerPosition = new Vector3(0f, -0.15f, 0.5f);
    }

    protected override void OnEnable()
    {
        SetVariables();
        base.OnEnable();
        leftHand.gameObject.SetActive(false);
    }
    protected override void Update()
    {

        if (Input.GetKeyDown(toggleControlHints))
        {
            showControlHints = !showControlHints;
            hintCanvas.SetActive(showControlHints);
        }

        if (Input.GetKeyDown(toggleMouseLock))
        {
            lockMouseToView = !lockMouseToView;
        }

        if (mouseMovementInput == MouseInputMode.RequiresButtonPress)
        {
            if (lockMouseToView)
            {
                Cursor.lockState = Input.GetKey(mouseMovementKey) ? CursorLockMode.Locked : CursorLockMode.None;
            }
            else if (Input.GetKeyDown(mouseMovementKey))
            {
                oldPos = Input.mousePosition;
            }
        }
        else
        {
            Cursor.lockState = lockMouseToView ? CursorLockMode.Locked : CursorLockMode.None;
        }

        //if (isHand)
        //{
        //    UpdateHands();
        //}
        //else
        //{
        UpdateRotation();
        if (Input.GetKeyDown(distancePickupRight) && Input.GetKey(distancePickupModifier))
        {
            TryPickup(true);
        }
        else if (Input.GetKeyDown(distancePickupLeft) && Input.GetKey(distancePickupModifier))
        {
            TryPickup(false);
        }
        if (Input.GetKey(sprint))
        {
            sprintMultiplier = playerSprintMultiplier;
        }
        else
        {
            sprintMultiplier = 1;
        }
        if (Input.GetKeyDown(distancePickupModifier))
        {
            crossHairPanel.SetActive(true);
        }
        else if (Input.GetKeyUp(distancePickupModifier))
        {
            crossHairPanel.SetActive(false);
        }
        //}

        // --------------------------------- IMPORTANT --------------------------------- 
        // Simulate the touch modifier
        // ----------------------------------------------------------------------------- 

        // Touchpad
        if (Input.GetKeyDown(touchpadTouch))
        {
            AuxInput.SetKeyDown(touchModifier);
            AuxInput.SetKeyDown(touchpadAlias);
        }
        else if (Input.GetKey(touchpadTouch))
        {
            AuxInput.SetKey(touchModifier);
            AuxInput.SetKey(touchpadAlias);
        }
        else if (Input.GetKeyUp(touchpadTouch))
        {
            AuxInput.SetKeyUp(touchModifier);
            AuxInput.SetKeyUp(touchpadAlias);
        }


        //Trigger
        if (Input.GetKeyDown(triggerTouch))
        {
            AuxInput.SetKeyDown(touchModifier);
            AuxInput.SetKeyDown(triggerAlias);
        }
        else if (Input.GetKey(triggerTouch))
        {
            AuxInput.SetKey(touchModifier);
            AuxInput.SetKey(triggerAlias);
        }
        else if (Input.GetKeyUp(triggerTouch))
        {
            AuxInput.SetKeyUp(touchModifier);
            AuxInput.SetKeyUp(triggerAlias);
        }

        UpdatePosition();

        if (showControlHints)
        {
            UpdateHints();
        }

        rightHand.position = neck.TransformPoint(controllerPosition);
        rightHand.rotation = neck.rotation;

        //if (Input.GetKeyDown(handsOnOff))
        //{
        //    if (isHand)
        //    {
        //        SetMove();
        //        ToggleGuidePlanes(false, false);
        //    }
        //    else
        //    {
        //        SetHand();
        //    }
        //}

        //if (Input.GetKeyDown(changeHands))
        //{
        //    if (currentHand.name == "LeftHand")
        //    {
        //        currentHand = rightHand;
        //        rightController.selected = true;
        //        leftController.selected = false;
        //    }
        //    else
        //    {
        //        currentHand = leftHand;
        //        rightController.selected = false;
        //        leftController.selected = true;
        //    }
        //}


    }

    protected override void UpdateHints()
    {
        string hints = "";
        Func<KeyCode, string> key = (k) => "<b>" + k.ToString() + "</b>";

        string mouseInputRequires = "";
        if (mouseMovementInput == MouseInputMode.RequiresButtonPress)
        {
            mouseInputRequires = " (" + key(mouseMovementKey) + ")";
        }

        // WASD Movement
        string movementKeys = moveForward.ToString() + moveLeft.ToString() + moveBackward.ToString() + moveRight.ToString();
        hints += "Toggle Control Hints: " + key(toggleControlHints) + "\n\n";
        hints += "Toggle Menu: " + key(toggleMouseLock) + "\n";
        hints += "Move Player/Playspace: <b>" + movementKeys + "</b>\n";
        hints += "Sprint Modifier: (" + key(sprint) + ")\n\n";

        // Controller Buttons
        //hints += "Touchpad Touch: " + key(touchpadTouch) + "\n";
        hints += "Activate/Deactivate Pointer: " + key(touchpadAlias) + "\n";

        //hints += "Trigger Touch: " + key(triggerTouch) + "\n";
        //hints += "Activate/Deactivate Pointer: " + key(triggerAlias) + "\n";

        hints += "Grab/Ungrab Object: " + key(gripAlias) + "\n";
        
        //hints += "Object Menu: " + key(buttonOneAlias) + "\n";
        
        //hints += "Button Two: " + key(buttonTwoAlias) + "\n";
        
        //hints += "Start Menu: " + key(startMenuAlias) + "\n";

        hintText.text = hints.TrimEnd();
    }
}
