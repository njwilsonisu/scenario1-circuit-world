﻿using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class ControllersManager : MonoBehaviour
{
    public GameObject rightController;
    public GameObject leftController;

    public List<Component> myComponents = new List<Component>();
    public List<Component> rightComponents = new List<Component>();
    public List<Component> leftComponents = new List<Component>();

    public VRTK_StraightPointerRenderer mine;
    public VRTK_StraightPointerRenderer right;
    public VRTK_StraightPointerRenderer left;

    public static float handleDistance = 0.25f;

    private void Awake()
    {
        // get the attached components
        GetAttachments();
        // copy the components and their values to the 2 controllers
        // IMPORTANT: The RuntimeComponentCopier currently doesnt support copying color values
        SetAttachments();
    }

    protected virtual void GetAttachments()
    {
        foreach (Component component in GetComponents<Component>())
        {
            if (component.GetType() != typeof(Transform) && component.GetType() != typeof(ControllersManager))
            {
                myComponents.Add(component);
            }
        }
    }

    protected virtual void SetAttachments()
    {
        rightController.SetActive(false);
        if (rightController != null)
        {
            foreach (Component component in myComponents)
            {
                var copy = rightController.AddComponent<Component>(component);
                rightComponents.Add(copy);

                //VRTK_SharedMethods.CloneComponent(component, rightController, true);
            }

        }
        rightController.SetActive(true);

        if (leftController != null)
        {
            leftController.SetActive(false);

            foreach (Component component in myComponents)
            {
                var copy = leftController.AddComponent(component);
                leftComponents.Add(copy);

                //Component copy = VRTK_SharedMethods.CloneComponent(component, leftController, true);
            }

            leftController.SetActive(false);
        }

        mine = GetComponent<VRTK_StraightPointerRenderer>();
        right = rightController.GetComponent<VRTK_StraightPointerRenderer>();
        left = leftController.GetComponent<VRTK_StraightPointerRenderer>();

        if (mine != null)
        {
            if (right != null)
            {
                CopyPointerRenderSettings(mine, right);
            }

            if (left != null)
            {
                CopyPointerRenderSettings(mine, left);
            }
        }
    }

    protected virtual void CopyPointerRenderSettings(VRTK_StraightPointerRenderer copyFrom, VRTK_StraightPointerRenderer copyTo)
    {
        copyTo.pointerOriginSmoothingSettings = copyFrom.pointerOriginSmoothingSettings;

        copyTo.validCollisionColor = copyFrom.validCollisionColor;
        copyTo.invalidCollisionColor = copyFrom.invalidCollisionColor;
    }
}
