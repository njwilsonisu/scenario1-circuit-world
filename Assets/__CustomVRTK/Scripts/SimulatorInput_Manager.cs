﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sets all the necessary variables, doesnt show all the unnecessary ones
/// </summary>
public class SimulatorInput_Manager : MonoBehaviour
{
    public SimulatorInput simulatorInput;

    // ----------------------------------- IMPORTANT VARIABLES ---------------------------------------------------

    [Tooltip("Show control information in the upper left corner of the screen.")]
    public bool showControlHints = true;
    //[Tooltip("Hide hands when disabling them.")]
    //public bool hideHandsAtSwitch = false;
    //[Tooltip("Reset hand position and rotation when enabling them.")]
    //public bool resetHandsAtSwitch = true;
    //[Tooltip("Displays an axis helper to show which axis the hands will be moved through.")]
    //public bool showHandAxisHelpers = true;

    [Header("Mouse Cursor Lock Settings")]

    [Tooltip("Lock the mouse cursor to the game window.")]
    public bool lockMouseToView = true;
    [Tooltip("Whether the mouse movement always acts as input or requires a button press.")]
    public SimulatorInput.MouseInputMode mouseMovementInput = SimulatorInput.MouseInputMode.Always;

    [Header("Manual Adjustment Settings")]

    //[Tooltip("Adjust hand movement speed.")]
    //public float handMoveMultiplier = 0.002f;
    //[Tooltip("Adjust hand rotation speed.")]
    //public float handRotationMultiplier = 0.5f;
    //[Tooltip("Adjust player movement speed.")]
    //public float playerMoveMultiplier = 5f;
    //[Tooltip("Adjust player rotation speed.")]
    //public float playerRotationMultiplier = 0.5f;
    //[Tooltip("Adjust player sprint speed.")]
    //public float playerSprintMultiplier = 2f;
    //[Tooltip("Adjust the speed of the cursor movement in locked mode.")]
    //public float lockedCursorMultiplier = 5f;
    //[Tooltip("The Colour of the GameObject representing the left hand.")]
    //public Color leftHandColor = Color.red;

    [Tooltip("The Colour of the GameObject representing the right hand.")]
    public Color handColor = Color.white;

    [Tooltip("The position of the controller relative to the Neck")]
    public Vector3 controllerPosition = new Vector3(0f, -0.15f, 0.5f);

    [Header("Operation Key Binding Settings")]

    //[Tooltip("Key used to enable mouse input if a button press is required.")]
    //public KeyCode mouseMovementKey = KeyCode.Mouse1;

    [Tooltip("Key used to toggle control hints on/off.")]
    public KeyCode toggleControlHints = KeyCode.F1;

    [Tooltip("Key used to toggle control hints on/off.")]
    public KeyCode toggleMouseLock = KeyCode.F4;

    [Tooltip("Key used to switch between left and righ hand.")]
    public KeyCode changeHands = KeyCode.Tab;

    [Tooltip("Key used to switch hands On/Off.")]
    public KeyCode handsOnOff = KeyCode.LeftAlt;

    [Tooltip("Key used to switch between positional and rotational movement.")]
    public KeyCode rotationPosition = KeyCode.LeftShift;

    [Tooltip("Key used to switch between X/Y and X/Z axis.")]
    public KeyCode changeAxis = KeyCode.LeftControl;

    [Tooltip("Key used to distance pickup with left hand.")]
    public KeyCode distancePickupLeft = KeyCode.Mouse0;

    [Tooltip("Key used to distance pickup with right hand.")]
    public KeyCode distancePickupRight = KeyCode.Mouse1;

    [Tooltip("Key used to enable distance pickup.")]
    public KeyCode distancePickupModifier = KeyCode.LeftControl;



    [Header("Movement Key Binding Settings")]

    [Tooltip("Key used to move forward.")]
    public KeyCode moveForward = KeyCode.W;

    [Tooltip("Key used to move to the left.")]
    public KeyCode moveLeft = KeyCode.A;

    [Tooltip("Key used to move backwards.")]
    public KeyCode moveBackward = KeyCode.S;

    [Tooltip("Key used to move to the right.")]
    public KeyCode moveRight = KeyCode.D;

    [Tooltip("Key used to sprint.")]
    public KeyCode sprint = KeyCode.LeftShift;

    [Header("Controller Key Binding Settings")]

    [Tooltip("Key used to simulate trigger touch.")]
    public KeyCode triggerTouch = KeyCode.None;

    [Tooltip("Key used to simulate trigger button.")]
    public KeyCode triggerAlias = KeyCode.Mouse1;

    [Tooltip("Key used to simulate touchpad touch.")]
    public KeyCode touchpadTouch = KeyCode.None;

    [Tooltip("Key used to simulate touchpad button.")]
    public KeyCode touchpadAlias = KeyCode.Q;

    [Tooltip("Key used to simulate grip button.")]
    public KeyCode gripAlias = KeyCode.Mouse0;



    [Tooltip("Key used to simulate button one.")]
    public KeyCode buttonOneAlias = KeyCode.E;

    [Tooltip("Key used to simulate button two.")]
    public KeyCode buttonTwoAlias = KeyCode.R;

    [Tooltip("Key used to simulate start menu button.")]
    public KeyCode startMenuAlias = KeyCode.F;

    [Tooltip("Key used to switch between button touch and button press mode.")]
    public KeyCode touchModifier = KeyCode.T;

    [Tooltip("Key used to switch between hair touch mode.")]
    public KeyCode hairTouchModifier = KeyCode.H;


    // ----------------------------------- ------------------- ---------------------------------------------------


    private void Awake()
    {
        SetVariables();
    }


    protected virtual void SetVariables()
    {
        // Unused

        simulatorInput.leftHandColor = Color.clear;
        simulatorInput.mouseMovementKey = KeyCode.None;
        simulatorInput.changeHands = KeyCode.None;
        simulatorInput.handsOnOff = KeyCode.None;
        simulatorInput.rotationPosition = KeyCode.None;
        simulatorInput.changeAxis = KeyCode.None;

        //purposefully obscure so the user does not touch them
        simulatorInput.touchModifier = touchModifier;
        simulatorInput.hairTouchModifier = hairTouchModifier;

        // ensure all the other variables are corrected
        simulatorInput.showControlHints = showControlHints;
        simulatorInput.lockMouseToView = lockMouseToView;
        simulatorInput.mouseMovementInput = mouseMovementInput;
        simulatorInput.rightHandColor = handColor;
        
        simulatorInput.distancePickupModifier = distancePickupModifier;
        simulatorInput.distancePickupRight = distancePickupRight;
        
        simulatorInput.distancePickupLeft = KeyCode.None;


        // Actual controls

        // Unused So Far
        simulatorInput.touchpadTouch = touchpadTouch;

        // Enable laser
        simulatorInput.touchpadAlias = touchpadAlias;
        // Grab
        simulatorInput.gripAlias = gripAlias;

        // Unused So Far
        simulatorInput.triggerTouch = triggerTouch;

        // Use/Select
        simulatorInput.triggerAlias = triggerAlias;

        //Open Object Menu
        simulatorInput.buttonOneAlias = buttonOneAlias;

        // Toggle Control Hints
        simulatorInput.toggleControlHints = toggleControlHints;

        // Toggle Menu
        simulatorInput.toggleMouseLock = toggleMouseLock;

        // No Use Yet
        simulatorInput.buttonTwoAlias = buttonTwoAlias;
        simulatorInput.startMenuAlias = startMenuAlias;

        simulatorInput.controllerPosition = controllerPosition;

    }
}
