﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using Photon.Pun;


public class NetworkVRTKDevice : MonoBehaviourPunCallbacks, IPunObservable
{
    // higher value -> faster update
    public float SmoothingDelay = 10;

    protected VRTK_DeviceFinder.Devices deviceType;

    public GameObject trackingModel;

    protected bool trackPosition = true;
    protected bool trackRotation = true;

    protected GameObject VRTKObject;

    // Networking
    protected Vector3 networkPosition;
    protected Quaternion networkRotation;


    protected virtual void Start()
    {
        if (trackingModel == null)
        {
            trackingModel = GetTrackingModel();
        }

        // if this is the local player...
        if (photonView.IsMine)
        {
            // subscribe to when the headset changes
            VRTK_SDKManager.AttemptAddBehaviourToToggleOnLoadedSetupChange(this);
        }
    }

    protected virtual GameObject GetTrackingModel()
    {
        return transform.GetChild(0).gameObject;
    }

    public override void OnEnable()
    {
        base.OnEnable();

        // if this is the local player...
        if (photonView.IsMine)
        {
            // disable the avatar object
            trackingModel.SetActive(false);

        }
        // if this is a network player
        else
        {
            // enable the avatar object
            trackingModel.SetActive(true);

        }
    }

    protected virtual GameObject GetVRTKObject()
    {
        return VRTK_DeviceFinder.DeviceTransform(deviceType).gameObject;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // THIS IS OUR LOCAL SCENE --> we need to send the information
            SendNetworkInfo(stream);

        }
        else
        {
            // THIS IS NOT OUR LOCAL SCENE --> we need to receive the information
            ReceiveNetworkInfo(stream);
        }
    }

    /// <summary>
    /// THIS IS OUR LOCAL SCENE --> we need to send the information
    /// </summary>
    protected virtual void SendNetworkInfo(PhotonStream stream)
    {
        stream.SendNext(networkPosition);
        stream.SendNext(networkRotation);
    }

    /// <summary>
    /// THIS IS NOT OUR LOCAL SCENE --> we need to receive the information
    /// </summary>
    protected virtual void ReceiveNetworkInfo(PhotonStream stream)
    {
        networkPosition = (Vector3)stream.ReceiveNext();
        networkRotation = (Quaternion)stream.ReceiveNext();
    }

    protected virtual void Update()
    {
        if (photonView.IsMine)
        {
            // THIS IS OUR LOCAL SCENE
            SetNetwork();
        }
        else
        {
            // NOT OUR LOCAL SCENE
            SetLocal();
        }
    }

    /// <summary>
    /// Used to set the network info based on the local information - PHOTON VIEW IS MINE
    /// </summary>
    protected virtual void SetNetwork()
    {
        // get the transform info for the VRTK object
        Vector3 vp = VRTKObject.transform.position;
        Quaternion vr = VRTKObject.transform.rotation;

        // copy it to the network info
        networkPosition = vp;
        networkRotation = vr;
    }

    /// <summary>
    /// Used to set the local info based on the network information - PHOTON VIEW IS NOT MINE
    /// </summary>
    protected virtual void SetLocal()
    {
        // move the object to the network pos and rot
        trackingModel.transform.position = Vector3.Lerp(trackingModel.transform.position, networkPosition, Time.deltaTime * SmoothingDelay);
        trackingModel.transform.rotation = Quaternion.Lerp(trackingModel.transform.rotation, networkRotation, Time.deltaTime * SmoothingDelay);
    }
}
