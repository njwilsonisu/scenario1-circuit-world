﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

[SDK_Description(typeof(SDK_SimSystem))]
public class SimulatorController : SDK_SimController
{
    // Modified to make use of the AuxInput
    protected override bool IsTouchModifierPressed()
    {
        //if (AuxInput.GetKey(VRTK_SharedMethods.GetDictionaryValue(keyMappings, "TouchModifier", KeyCode.None)))
        //{
        //    Debug.LogError("Touching");
        //}
        return AuxInput.GetKey(VRTK_SharedMethods.GetDictionaryValue(keyMappings, "TouchModifier", KeyCode.None));
    }

    // Modified to make use of the AuxInput
    protected override bool IsHairTouchModifierPressed()
    {
        //if (AuxInput.GetKey(VRTK_SharedMethods.GetDictionaryValue(keyMappings, "TouchModifier", KeyCode.None)))
        //{
        //    Debug.Log("Hair Touching");
        //}
        return AuxInput.GetKey(VRTK_SharedMethods.GetDictionaryValue(keyMappings, "HairTouchModifier", KeyCode.None));
    }

    /// <summary>
    /// checks if the given button (KeyCode) is currently in a specific pressed state (ButtonPressTypes) on the keyboard
    /// also asserts that button presses are only handled for the currently active controller by comparing the controller indices
    /// </summary>
    /// <param name="index">unique index of the controller for which the button press is to be checked</param>
    /// <param name="type">the type of press (up, down, hold)</param>
    /// <param name="button">the button on the keyboard</param>
    /// <returns>Returns true if the button is being pressed.</returns>
    protected override bool IsButtonPressed(uint index, ButtonPressTypes type, KeyCode button)
    {
        SetupPlayer();
        if (index >= uint.MaxValue)
        {
            return false;
        }

        if (index == 1)
        {
            if (rightController == null || !rightController.selected)
            {
                return false;
            }
        }
        else if (index == 2)
        {
            if (leftController == null || !leftController.selected)
            {
                return false;
            }
        }
        else
        {
            return false;
        }

        switch (type)
        {
            case ButtonPressTypes.Press:
                return Input.GetKey(button);
            case ButtonPressTypes.PressDown:
                return AuxInput.GetKeyDown(button);
            case ButtonPressTypes.PressUp:
                return AuxInput.GetKeyUp(button);
        }
        return false;
    }
}
