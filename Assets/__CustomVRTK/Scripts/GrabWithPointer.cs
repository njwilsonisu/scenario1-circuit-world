﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class GrabWithPointer : MonoBehaviour
{
    public float grabDistance = 0.25f;

    protected float defaultDistance;

    protected VRTK_InteractGrab interactGrab;

    protected VRTK_StraightPointerRenderer pointerRenderer;

    private void Awake()
    {
        if (interactGrab == null)
        {
            interactGrab = gameObject.GetComponent<VRTK_InteractGrab>();
        }

        if (pointerRenderer == null)
        {
            pointerRenderer = gameObject.GetComponent<VRTK_StraightPointerRenderer>();
            defaultDistance = pointerRenderer.maximumLength;
        }
    }

    private void OnEnable()
    {
        interactGrab.ControllerStartGrabInteractableObject += OnObjectGrabbed;
        interactGrab.ControllerUngrabInteractableObject += OnObjectDropped;
    }

    protected void OnObjectGrabbed(object sender, ObjectInteractEventArgs e)
    {
        pointerRenderer.maximumLength = grabDistance;
    }

    protected void OnObjectDropped(object sender, ObjectInteractEventArgs e)
    {
        pointerRenderer.maximumLength = defaultDistance;
    }
}
