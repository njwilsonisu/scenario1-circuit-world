﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using VRTK;

[CustomEditor(typeof(VRTK_SnapDropZone), true)]
public class SnapDropZoneEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        VRTK_SnapDropZone t = (VRTK_SnapDropZone)target;
        if (GUILayout.Button("Refresh Prefab"))
        {
            GameObject g = t.highlightObjectPrefab;
            t.highlightObjectPrefab = null;
            t.InitaliseHighlightObject();
            t.highlightObjectPrefab = g;
        }
    }
}
