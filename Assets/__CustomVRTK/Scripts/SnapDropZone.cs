﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class SnapDropZone : VRTK_SnapDropZone
{

    // Might have to do something with this

    protected override void OnDrawGizmosSelected()
    {
        // Nothing
    }

    public virtual void ListenForObjectSnapped(SnapDropZoneEventHandler s, bool listen)
    {
        if (listen)
        {
            ObjectSnappedToDropZone += s;
        }
        else
        {
            ObjectSnappedToDropZone -= s;
        }
    }

    public virtual void ListenForObjectUnsnapped(SnapDropZoneEventHandler s, bool listen)
    {
        if (listen)
        {
            ObjectUnsnappedFromDropZone += s;
        }
        else
        {
            ObjectUnsnappedFromDropZone -= s;
        }
    }
}
