﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using VRTK;
using VRTK.GrabAttachMechanics;

public class GrabMechanic_Move : VRTK_MoveTransformGrabAttach
{
    public void Move()
    {
        Limits2D l = new Limits2D(-Mathf.Infinity, Mathf.Infinity);
        ConfigureMechanic(l, l, l);
    }

    public void DoNotMove()
    {
        Limits2D l = new Limits2D(-Mathf.Infinity, Mathf.Infinity);
        //Limits2D l = Limits2D.zero;
        ConfigureMechanic(l, l, l);
    }

    public void ConfigureMechanic(Limits2D x = null, Limits2D y = null, Limits2D z = null)
    {
        xAxisLimits = x ?? new Limits2D(-Mathf.Infinity, Mathf.Infinity);
        yAxisLimits = y ?? new Limits2D(-Mathf.Infinity, Mathf.Infinity);
        zAxisLimits = z ?? new Limits2D(-Mathf.Infinity, Mathf.Infinity);

        detachDistance = Mathf.Infinity;

        trackingSpeed = 50f;

        kinematic = false;
        forceKinematicOnGrab = false;

        precisionGrab = true;

        ResetState();
    }

}
