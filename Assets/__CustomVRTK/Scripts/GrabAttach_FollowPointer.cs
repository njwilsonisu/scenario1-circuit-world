﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.GrabAttachMechanics;

public class GrabAttach_FollowPointer : VRTK_BaseGrabAttach
{
    public float snapDuration = 0.2f;

    protected float handleDistance;

    public bool follow = false;

    /// <summary>
    /// The StartGrab method sets up the grab attach mechanic as soon as an Interactable Object is grabbed. It is also responsible for creating the joint on the grabbed object.
    /// </summary>
    /// <param name="grabbingObject">The GameObject that is doing the grabbing.</param>
    /// <param name="givenGrabbedObject">The GameObject that is being grabbed.</param>
    /// <param name="givenControllerAttachPoint">The point on the grabbing object that the grabbed object should be attached to after grab occurs.</param>
    /// <returns>Returns `true` if the grab is successful, `false` if the grab is unsuccessful.</returns>
    public override bool StartGrab(GameObject grabbingObject, GameObject givenGrabbedObject, Rigidbody givenControllerAttachPoint)
    {
        if (base.StartGrab(grabbingObject, givenGrabbedObject, givenControllerAttachPoint))
        {
            SnapObjectToGrabToController(givenGrabbedObject);
            grabbedObjectScript.isKinematic = true;
            return true;
        }
        return false;
    }

    /// <summary>
    /// The StopGrab method ends the grab of the current Interactable Object and cleans up the state.
    /// </summary>
    /// <param name="applyGrabbingObjectVelocity">If `true` will apply the current velocity of the grabbing object to the grabbed object on release.</param>
    public override void StopGrab(bool applyGrabbingObjectVelocity)
    {
        ReleaseObject(applyGrabbingObjectVelocity);
        base.StopGrab(applyGrabbingObjectVelocity);
    }

    protected override void Initialise()
    {
        tracked = false;
        climbable = false;
        kinematic = true;
    }


    protected virtual void SnapObjectToGrabToController(GameObject obj)
    {
        //  --------------------------------------- Old Way ---------------------------------------
        //SetSnappedObjectPosition(obj);
        //obj.transform.SetParent(controllerAttachPoint.transform);
        //  ---------------------------------------------------------------------------------------

        handleDistance = ControllersManager.handleDistance;

        // New way
        StartCoroutine(MoveThenParent(obj));
    }

    // Moves the object to a certain distance away from the controller over time
    protected virtual IEnumerator MoveThenParent(GameObject obj)
    {
        float elapsedTime = 0f;

        Vector3 startPosition = obj.transform.position;
        Quaternion startRotation = obj.transform.rotation;

        Vector3 targetPosition;
        Quaternion targetRotation;

        while (elapsedTime <= snapDuration)
        {
            elapsedTime += Time.deltaTime;

            targetPosition = controllerAttachPoint.transform.position + controllerAttachPoint.transform.forward * handleDistance;
            targetRotation = Quaternion.LookRotation(controllerAttachPoint.transform.forward);

            obj.transform.position = Vector3.Lerp(startPosition, targetPosition, (elapsedTime / snapDuration));
            obj.transform.rotation = Quaternion.Lerp(startRotation, targetRotation, (elapsedTime / snapDuration));
            yield return null;
        }

        //Force all to the last setting in case anything has obj.transformd during the transition
        obj.transform.position = controllerAttachPoint.transform.position + controllerAttachPoint.transform.forward * handleDistance;
        obj.transform.rotation = Quaternion.LookRotation(controllerAttachPoint.transform.forward);

        // The object is in the right position - parent it under the controller
        obj.transform.SetParent(controllerAttachPoint.transform);
    }


    protected virtual void Update()
    {
        
    }


    // OLD

    protected virtual void SetSnappedObjectPosition(GameObject obj)
    {
        if (grabbedSnapHandle == null)
        {
            obj.transform.position = controllerAttachPoint.transform.position;
        }
        else
        {
            obj.transform.rotation = controllerAttachPoint.transform.rotation * Quaternion.Inverse(grabbedSnapHandle.transform.localRotation);
            obj.transform.position = controllerAttachPoint.transform.position - (grabbedSnapHandle.transform.position - obj.transform.position);
        }
    }
}
