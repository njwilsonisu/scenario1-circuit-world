﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class RotateTowardsHeadset : MonoBehaviour
{
    public Transform headset;

    public bool follow = true;

    private void OnEnable()
    {
        GetHeadset();
    }

    public void GetHeadset()
    {
        headset = VRTK_DeviceFinder.DeviceTransform(VRTK_DeviceFinder.Devices.Headset);
    }

    private void Update()
    {
        if(headset != null && follow)
        {
            transform.LookAt(headset);
        }
        
    }
}
