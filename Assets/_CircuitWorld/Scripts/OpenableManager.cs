﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenableManager : MonoBehaviour
{
    protected List<Openable> openables = new List<Openable>();

    private void Awake()
    {
        if(openables == null || openables.Count == 0)
        {
            openables.AddRange(gameObject.GetComponentsInChildren<Openable>());
        }
    }

    private void OnEnable()
    {
        foreach(Openable o in openables)
        {
            o.Opened += OnDrawerOpened;
        }
    }

    private void OnDisable()
    {
        foreach (Openable o in openables)
        {
            o.Opened -= OnDrawerOpened;
        }
    }

    protected virtual void OnDrawerOpened(Openable openable)
    {
        foreach (Openable o in openables)
        {
            if(o != openable && o.open == true)
            {
                o.ToggleOpen();
            }
            
        }
    }
}
