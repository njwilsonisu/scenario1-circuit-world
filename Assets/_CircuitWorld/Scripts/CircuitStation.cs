﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class CircuitStation : MonoBehaviour
{
    // Where new circuit simulators can be placed
    public SnapDropZone circuitDropZone;

    public GameObject diagnosticTool;

    protected CircuitSimulator circuitSimulator;

    protected virtual void OnEnable()
    {
        circuitDropZone.ListenForObjectSnapped(OnCircuitSnapped, true);
    }

    protected virtual void OnDisable()
    {
        circuitDropZone.ListenForObjectSnapped(OnCircuitSnapped, false);
    }

    protected virtual void OnCircuitSnapped(object sender, SnapDropZoneEventArgs e)
    {
        // get the circuit simulator
        circuitSimulator = e.snappedObject.GetComponent<CircuitSimulator>();
        if(circuitSimulator != null)
        {
            circuitSimulator.InitializeCircuit();
        }

        e.snappedObject.transform.DOScale(1f, 1f);

        diagnosticTool.SetActive(true);
    }

}
