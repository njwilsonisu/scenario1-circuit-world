﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public delegate void OpenableEventHandler(Openable sender);

public class Openable : MonoBehaviour
{
    // The objects that are contained in this openable
    public GameObject containedObjectPrefab;
    public GameObject contentsHolder;

    protected VRTK_InteractableObject openableObject;
    protected Animator animator;

    protected VRTK_InteractableObject containedObject;

    [HideInInspector]
    public bool open = false;

    public event OpenableEventHandler Opened;

    private void Awake()
    {
        if (openableObject == null)
        {
            openableObject = GetComponent<VRTK_InteractableObject>();
        }

        if (animator == null)
        {
            animator = GetComponent<Animator>();
        }

        if (containedObjectPrefab != null)
        {
            CreateNewContainedObject();
        }
    }

    private void OnEnable()
    {
        // Listen for use
        openableObject.InteractableObjectUsed += OnUse;
    }

    private void OnDisable()
    {
        openableObject.InteractableObjectUsed -= OnUse;

        if (containedObject != null)
        {
            containedObject.InteractableObjectGrabbed -= OnContentsGrabbed;
        }
    }

    public virtual void OnUse(object sender, InteractableObjectEventArgs e)
    {
        if (Opened != null)
        {
            Opened(this);
        }

        if (containedObject != null && containedObject.gameObject.activeInHierarchy == false)
        {
            containedObject.gameObject.SetActive(true);
        }

        ToggleOpen();
    }

    public virtual void ToggleOpen()
    {
        open = !open;

        // toggle the bool open
        animator.SetBool("Open", open);
    }

    public virtual void OnContentsGrabbed(object sender, InteractableObjectEventArgs e)
    {
        // Close the drawer
        ToggleOpen();

        // Create new contents
        CreateNewContainedObject();
    }

    protected virtual void CreateNewContainedObject()
    {
        // stop listening to the old contained object
        if (containedObject != null)
        {
            containedObject.InteractableObjectGrabbed -= OnContentsGrabbed;
        }

        // create the new object
        GameObject n = Instantiate(containedObjectPrefab, contentsHolder.transform);

        // disable the object
        n.SetActive(false);

        // reset the containedObject variable
        containedObject = n.GetComponent<VRTK_InteractableObject>();

        // start listening
        containedObject.InteractableObjectGrabbed += OnContentsGrabbed;
    }

    //public GameObject contents;
    //protected Vector3 startPosition;
    //public Vector3 endPosition;
    //public float delay = 0.25f;
    //public float duration = 0.25f;

    //public IEnumerator TweenLocal(GameObject obj, Vector3 endPosition, float duration)
    //{
    //    yield return new WaitForSeconds(delay);

    //    float elapsedTime = 0f;

    //    Transform ioTransform = obj.transform;

    //    Vector3 startPosition = ioTransform.localPosition;

    //    while (elapsedTime <= duration)
    //    {
    //        elapsedTime += Time.deltaTime;

    //        ioTransform.localPosition = Vector3.Lerp(startPosition, endPosition, (elapsedTime / duration));

    //        yield return null;
    //    }

    //    //Force all to the last setting in case anything has moved during the transition
    //    ioTransform.localPosition = endPosition;
    //}
}
