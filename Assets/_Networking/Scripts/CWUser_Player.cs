﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace CW
{
    public abstract class CWUser_Player : CWUser_Base
    {
        // this is used for actual players that interact with circuit world either through desktop pc or a VR headset

        public JoinCircuitWorldUI joinCircuitWorldUI;

        [Header("Player Objects")]
        public GameObject VRTKPlayerPrefab;

        // the player instance
        protected GameObject VRTKPlayer;

        public override void InitializeLocalUser()
        {
            Debug.LogError("1");
            // disable the network objects
            userNetworkObjects.SetActive(false);

            // open the UI
            OpenJoinCircuitWorldUI(true);
        }

        public void OpenJoinCircuitWorldUI(bool open)
        {
            if (open)
            {
                joinCircuitWorldUI.joinCircuitWorldButton.onClick.AddListener(JoinCircuitWorld);
            }
            else
            {
                joinCircuitWorldUI.joinCircuitWorldButton.onClick.RemoveListener(JoinCircuitWorld);
            }
            
            joinCircuitWorldUI.gameObject.SetActive(open);
            ActivateUICamera(open);
        }

        // This function will be called once the starting UI button is pressed
        public void JoinCircuitWorld()
        {
            // close the UI
            OpenJoinCircuitWorldUI(false);

            // Initialize the player
            InitializeVRTKPlayer();

            // enable the network objects
            userNetworkObjects.SetActive(true);
        }

        protected virtual void InitializeVRTKPlayer()
        {
            // instantiate the player
            VRTKPlayer = Instantiate(VRTKPlayerPrefab, transform);

            // Get all the SDKs
            VRTK_SDKSetup[] setups = VRTK_SDKManager.GetAllSDKSetups();

            // Get the chosen SDK
            int sdk = joinCircuitWorldUI.SDKTypeButtonGroup.currentSelection + 1;

            // try loading the chosen SDK
            VRTK_SDKManager.AttemptTryLoadSDKSetup(sdk, true, setups);
        }
    }
}