﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExitGames.Client.Photon;
using Photon.Realtime;
using Photon.Pun;
using TMPro;
using UnityEngine.UI;
using VRTK;


namespace CW
{
    public class JoinCircuitWorldUI : MonoBehaviour
    {
        public GameObject waitingScreen;
        public GameObject playerSetupScreen;
        public ButtonGroup SDKTypeButtonGroup;
        public GameObject otherPlayerStatusObject;
        public Button joinCircuitWorldButton;

        private bool SDKSelected = false;
        private bool waitForOtherPlayerToConnect = true;
        private bool otherPlayerConnected = false;

        private CWUserType localUserType;

        private TextMeshProUGUI otherPlayerLabel;
        private TextMeshProUGUI otherPlayerStatus;

        private void Start()
        {
            otherPlayerLabel = otherPlayerStatusObject.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            otherPlayerStatus = otherPlayerStatusObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>();

            localUserType = CWNetworkManager.instance.userType;

            if(localUserType == CWUserType.Apprentice)
            {
                waitForOtherPlayerToConnect = true;
                otherPlayerLabel.text = "Instructor:";
            }
            else if (localUserType == CWUserType.Instructor)
            {
                waitForOtherPlayerToConnect = false;
                otherPlayerLabel.text = "Apprentice:";
            }
        }

        private void OnEnable()
        {
            OpenWaitingScreen();
            SDKTypeButtonGroup.gameObject.SetActive(false);

            PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
            SDKTypeButtonGroup.SelectionChanged += OnSDKSelected;
        }

        private void OnDisable()
        {
            PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;

            SDKTypeButtonGroup.SelectionChanged -= OnSDKSelected;
        }

        private void OpenWaitingScreen()
        {
            playerSetupScreen.SetActive(false);
            waitingScreen.SetActive(true);
        }

        private void OpenPlayerSetupScreen()
        {
            waitingScreen.SetActive(false);
            playerSetupScreen.SetActive(true);

            CWPlayerPlatform playerPlatform = CWNetworkManager.instance.playerPlatform;

            if(playerPlatform == CWPlayerPlatform.VR)
            {
                CreateSDKButtons();
            }
        }

        private void SetOtherPlayerStatus(bool connected)
        {
            if (connected)
            {
                otherPlayerStatus.text = "Connected";
                otherPlayerStatus.color = Color.green;
            }
            else
            {
                otherPlayerStatus.text = "Not Connected";
                otherPlayerStatus.color = Color.red;
            }
        }

        private void CreateSDKButtons()
        {
            // get all of the possible sdks
            VRTK_SDKSetup[] setups = VRTK_SDKManager.GetAllSDKSetups();

            for(int i = 1; i < setups.Length; i++)
            {
                SDKTypeButtonGroup.AddButton(setups[i].name);
            }

            SDKTypeButtonGroup.gameObject.SetActive(true);

        }

        private void OnSDKSelected()
        {
            SDKSelected = true;
            AttemptEnableButton();
        }

        private void OnInstructorConnected()
        {
            if(localUserType == CWUserType.Apprentice)
            {
                otherPlayerConnected = true;
                SetOtherPlayerStatus(true);
                AttemptEnableButton();
            }
        }

        private void OnApprenticeConnected()
        {
            if (localUserType == CWUserType.Instructor)
            {
                otherPlayerConnected = true;
                SetOtherPlayerStatus(true);
                AttemptEnableButton();
            }
        }

        private void AttemptEnableButton()
        {
            if(SDKSelected && (otherPlayerConnected || !waitForOtherPlayerToConnect))
            {
                joinCircuitWorldButton.interactable = true;
            }
        }

        public void OnEvent(EventData photonEvent)
        {
            byte eventCode = photonEvent.Code;

            if (eventCode == CWNetworkManager.instance.SetApprenticeEventCode)
            {
                OnApprenticeConnected();
            }

            if (eventCode == CWNetworkManager.instance.SetInstructorEventCode)
            {
                OnInstructorConnected();
            }

            if (eventCode == CWNetworkManager.instance.SetCWParametersEventCode)
            {
                OpenPlayerSetupScreen();
            }
        }
    }
}