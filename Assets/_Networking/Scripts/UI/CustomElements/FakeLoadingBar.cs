﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class OnDoneLoading : UnityEvent { };

public class FakeLoadingBar : MonoBehaviour
{
    public float loadingTime = 2f;
    
    public float speedMultiplier = 0.01f;

    private GameObject fillBar;

    public OnDoneLoading onDoneLoading;

    private void OnEnable()
    {
        StartLoading();
    }

    public void StartLoading()
    {
        if (fillBar == null)
        {
            fillBar = transform.GetChild(0).gameObject;
        }

        fillBar.SetActive(true);
        fillBar.transform.localScale = new Vector3(0f, 1f, 1f);
        StartCoroutine(LoadSpeed());
    }

    public IEnumerator LoadSpeed()
    {
        float elapsedTime = 0f;
        float progress = 0f;
        float speed;

        while (progress < 1.0f)
        {
            elapsedTime += Time.deltaTime;

            speed = UnityEngine.Random.Range(0f, speedMultiplier);

            progress = Mathf.MoveTowards(progress, 1f, elapsedTime * speed);

            fillBar.transform.localScale = new Vector3(progress, 1f, 1f);

            yield return null;
        }

        if(onDoneLoading != null)
        {
            onDoneLoading.Invoke();
        }

    }

    public IEnumerator LoadTime()
    {
        float elapsedTime = 0f;
        float progress = 0f;
        float speed;

        while (progress < 1.0f)
        {
            elapsedTime += Time.deltaTime;
            speed = UnityEngine.Random.Range(0f, speedMultiplier);
            progress = Mathf.Lerp(progress, 1.0f, speed);
            yield return null;
        }

        if (onDoneLoading != null)
        {
            onDoneLoading.Invoke();
        }

    }

}
