﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace CW
{
    public delegate void ButtonGroupEventHandler();
    public class ButtonGroup : MonoBehaviour
    {
        public GameObject buttonsHolder;

        public GameObject standardButtonPrefab;

        public List<Button> buttons = new List<Button>();

        public bool startWithDefault = true;
        public int defaultSelection = 0;

        public int currentSelection = -1;

        private bool listening = false;

        public event ButtonGroupEventHandler SelectionChanged;

        private void Awake()
        {
            if (buttons.Count > 0 && standardButtonPrefab == null)
            {
                standardButtonPrefab = buttons[0].gameObject;
            }

            if (buttonsHolder == null)
            {
                buttonsHolder = gameObject;
            }
        }

        private void Start()
        {
            if (startWithDefault)
            {
                OnButtonPressed(defaultSelection);
            }
        }

        private void OnEnable()
        {
            ListenToAllButtons(true);
        }

        private void OnDisable()
        {
            ListenToAllButtons(false);
        }

        public void OnButtonPressed(int index)
        {
            currentSelection = index;
            SetToggled(buttons[currentSelection]);

            for (int i = 0; i < buttons.Count; i++)
            {
                if (i != currentSelection)
                {
                    SetUntoggled(buttons[i]);
                }
            }

            if (SelectionChanged != null)
            {
                SelectionChanged();
            }
        }

        private void SetToggled(Button button)
        {
            button.interactable = false;
        }

        private void SetUntoggled(Button button)
        {
            button.interactable = true;
        }

        public void AddButton(string text)
        {
            if((buttons == null || buttons.Count == 0) && standardButtonPrefab != null)
            {
                standardButtonPrefab.SetActive(false);
            }

            Button newButton = Instantiate(standardButtonPrefab, buttonsHolder.transform).GetComponent<Button>();
            newButton.name = text;
            newButton.GetComponentInChildren<TextMeshProUGUI>().text = text;

            // add the new button to the list
            buttons.Add(newButton);

            // if we are already listening to all the other buttons...
            if (listening)
            {
                // we need to add the listener to this button
                newButton.onClick.AddListener(() => OnButtonPressed(buttons.Count - 1));
            }
        }

        private void ListenToAllButtons(bool listen)
        {
            if (listen)
            {
                for (int i = 0; i < buttons.Count; i++)
                {
                    int index = i; // Capture
                    buttons[index].onClick.AddListener(() => OnButtonPressed(index));
                }
            }
            else
            {
                for (int i = 0; i < buttons.Count; i++)
                {
                    int index = i; // Capture
                    buttons[index].onClick.RemoveAllListeners();
                }
            }

            listening = listen;
        }
    }
}