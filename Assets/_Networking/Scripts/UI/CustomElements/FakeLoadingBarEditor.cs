﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FakeLoadingBar))]
public class FakeLoadingBarEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        FakeLoadingBar t = (FakeLoadingBar)target;
        if (GUILayout.Button("Test Loading"))
        {
            t.StartLoading();
        }
    }
}
