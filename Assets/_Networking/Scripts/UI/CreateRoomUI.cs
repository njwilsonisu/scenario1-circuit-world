﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace CW
{
    public class CreateRoomUI : MonoBehaviour
    {
        [HideInInspector]
        public CWNetworkLauncher networkLauncher;

        public TMP_InputField roomIDInputField;
        public Button copyRoomIDButton;
        public Button createButton;
        public GameObject failMessage;

        private void OnEnable()
        {
            if (networkLauncher == null)
            {
                networkLauncher = CWNetworkLauncher.instance;
            }

            failMessage.SetActive(false);

            // listen
            copyRoomIDButton.onClick.AddListener(CopyRoomIDToClipboard);
            createButton.onClick.AddListener(OnCreateButtonPressed);

            GenerateRoomID();

        }

        private void OnDisable()
        {
            copyRoomIDButton.onClick.RemoveListener(CopyRoomIDToClipboard);
            createButton.onClick.RemoveListener(OnCreateButtonPressed);
        }

        private void GenerateRoomID()
        {
            // generate a random string
            string roomID = PasswordGenerator.GeneratePassword(10);

            // update the RoomID input field text
            roomIDInputField.SetTextWithoutNotify(roomID);
        }

        public void CopyRoomIDToClipboard()
        {
            TextEditor t = new TextEditor();
            t.text = roomIDInputField.text;
            t.SelectAll();
            t.Copy();
        }

        public void OnCreateButtonPressed()
        {
            failMessage.SetActive(false);

            networkLauncher.AttemptCreateRoom(roomIDInputField.text);
        }

        public void OnCreateRoomFailed()
        {
            failMessage.SetActive(true);
        }
    }
}