﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace CW
{
    public class StartCircuitWorldUI : MonoBehaviour
    {
        public ButtonGroup playerPlatformButtonGroup;
        public TMP_InputField participantIDInputField;
        public Button startCircuitWorldButton;

        private void OnEnable()
        {
            // listen
            participantIDInputField.onValueChanged.AddListener(OnRoomIDValueChanged);
        }

        private void OnDisable()
        {
            participantIDInputField.onValueChanged.RemoveListener(OnRoomIDValueChanged);
        }

        public virtual void OnRoomIDValueChanged(string input)
        {
            if (input.Length > 0)
            {
                startCircuitWorldButton.interactable = true;
            }
            else
            {
                startCircuitWorldButton.interactable = false;
            }
        }
    }
}