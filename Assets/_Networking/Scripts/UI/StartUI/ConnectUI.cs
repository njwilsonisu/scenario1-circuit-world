﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace CW
{
    public delegate void ConnectUIEventHandler(string userID);

    public class ConnectUI : MonoBehaviour
    {
        public TMP_InputField userIDInputField;
        
        public Button connectButton;

        public event ConnectUIEventHandler AttemptToConnect;

        protected virtual void OnEnable()
        {
            userIDInputField.onValueChanged.AddListener(OnUserIDValueChanged);

            connectButton.onClick.AddListener(() => OnConnectButtonPressed());

            connectButton.interactable = false;
        }

        public virtual void OnUserIDValueChanged(string input)
        {
            if(input.Length > 0)
            {
                connectButton.interactable = true;
            }
            else
            {
                connectButton.interactable = false;
            }
        }

        public virtual void OnConnectButtonPressed()
        {
            bool attempt = true;

            if (userIDInputField.text.Equals(""))
            {
                attempt = false;
                //----------------------------------
                // prompt user to enter userID
                //----------------------------------
            }

            if (attempt)
            {
                if(AttemptToConnect != null)
                {
                    AttemptToConnect(userIDInputField.text);
                }
            }
        }
    }
}