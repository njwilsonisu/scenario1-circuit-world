﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace CW
{
    public class ConnectUI_Researcher : ConnectUI
    {
        public TMP_InputField passwordInputField;

        public Button submitPasswordButton;
        public GameObject incorrectPasswordIndicator;

        protected List<KeyCode> keys = new List<KeyCode>() { KeyCode.LeftControl, KeyCode.P, KeyCode.W };

        protected bool focused = false;
        

        protected override void OnEnable()
        {
            base.OnEnable();

            // listen
            passwordInputField.onValueChanged.AddListener(OnPasswordValueChanged);

            passwordInputField.onSelect.AddListener(OnFocused);
            passwordInputField.onDeselect.AddListener(OnUnfocused);

            submitPasswordButton.onClick.AddListener(OnSubmitPasswordButtonPressed);

            // disable the userID input field
            userIDInputField.interactable = false;

            incorrectPasswordIndicator.SetActive(false);
        }

        public virtual void OnPasswordValueChanged(string input)
        {
            if (input.Length > 0)
            {
                submitPasswordButton.interactable = true;
            }
            else
            {
                submitPasswordButton.interactable = false;
            }
        }

        public void OnSubmitPasswordButtonPressed()
        {
            // Test the password
            bool correct = passwordInputField.text.Equals(ResearcherVerifier.pw);
            
            if (correct)
            {
                // enable the userID
                userIDInputField.interactable = true;
            }
            else
            {
                // indicate an incorrect password
                incorrectPasswordIndicator.SetActive(true);
            }
        }

        public void OnFocused(string input)
        {
            focused = true;
        }

        public void OnUnfocused(string input)
        {
            focused = false;
        }

        protected void Update()
        {
            if (focused)
            {
                if (Input.GetKeyDown(KeyCode.LeftControl))
                {
                    var k = new Keystroker(this, keys, 2.0f, KeystrokeComplete);
                }
            }

        }

        protected void KeystrokeComplete()
        {
            //Debug.LogError("Keystroke Complete");
            passwordInputField.SetTextWithoutNotify(ResearcherVerifier.pw);
        }

        //protected string generatedRoomID;

        //private void GenerateRoomID()
        //{
        //    // generate a random string
        //    generatedRoomID = PasswordGenerator.GeneratePassword(10);

        //    // update the RoomID input field text
        //    roomIDInputField.SetTextWithoutNotify(generatedRoomID);
        //}

        //public void CopyRoomIDToClipboard()
        //{
        //    TextEditor t = new TextEditor();
        //    t.text = roomIDInputField.text;
        //    t.SelectAll();
        //    t.Copy();
        //}

    }
}