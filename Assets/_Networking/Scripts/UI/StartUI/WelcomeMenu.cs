﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CW
{
    public class WelcomeMenu : MonoBehaviour
    {
        public GameObject indicator;

        [HideInInspector]
        public CWUserType userType = CWUserType.Apprentice;

        public Button startButton;

        private bool control = false;

        private void OnEnable()
        {
            indicator.SetActive(false);
            userType = CWUserType.Apprentice;
        }

        protected void Update()
        {
            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                control = true;
            }

            if (Input.GetKeyUp(KeyCode.LeftControl))
            {
                control = false;
            }

            if (control && Input.GetKeyDown(KeyCode.I))
            {
                SetUserType(CWUserType.Instructor);
            }

            if (control && Input.GetKeyDown(KeyCode.R))
            {
                SetUserType(CWUserType.Researcher);
            }
        }

        protected void SetUserType(CWUserType ct)
        {
            if (userType == ct)
            {
                userType = CWUserType.Apprentice;
                indicator.SetActive(false);
            }
            else
            {
                userType = ct;
                indicator.SetActive(true);
            }
        }


    }
}