﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CW
{
    public class StartUI : MonoBehaviour
    {
        public CWNetworkLauncher networkLauncher;

        public WelcomeMenu welcomeMenu;
        public List<ConnectUI> connectUIs;
        public Button homeButton;
        public TextMeshProUGUI connectFailMessage;

        [Header("User Info")]
        public string userID;
        public CWUserType userType = CWUserType.Apprentice;

        public void OnEnable()
        {
            networkLauncher = CWNetworkLauncher.instance;

            // listen
            welcomeMenu.startButton.onClick.AddListener(OnStartButtonPressed);
            homeButton.onClick.AddListener(OnHomeButtonPressed);

            foreach(ConnectUI connectUI in connectUIs)
            {
                connectUI.AttemptToConnect += OnAttemptToConnect;
            }

            OpenWelcomeMenu();
        }

        public void OpenWelcomeMenu()
        {
            foreach (ConnectUI connectUI in connectUIs)
            {
                connectUI.gameObject.SetActive(false);
            }

            welcomeMenu.gameObject.SetActive(true);
        }

        public void OnStartButtonPressed()
        {
            // close the welcome menu
            welcomeMenu.gameObject.SetActive(false);

            // get the user type from the welcome menu
            userType = welcomeMenu.userType;

            // open the corresponding menu
            OpenConnectUI(userType);
        }

        public void OpenConnectUI(CWUserType userType)
        {
            int i = (int)userType;
            
            connectUIs[i].gameObject.SetActive(true);            
        }

        public void OnHomeButtonPressed()
        {
            OpenWelcomeMenu();
        }

        public void OnAttemptToConnect(string userID)
        {
            this.userID = userID;
            networkLauncher.AttemptToConnect(userID, userType);
        }

        public void FailedToConnect()
        {
            // do something
        }
    }
}