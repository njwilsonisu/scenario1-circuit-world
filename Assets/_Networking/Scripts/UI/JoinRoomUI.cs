﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace CW
{
    public class JoinRoomUI : MonoBehaviour
    {
        [HideInInspector]
        public CWNetworkLauncher networkLauncher;

        public TMP_InputField roomIDInputField;
        public Button joinButton;
        public GameObject failMessage;

        private void OnEnable()
        {
            if (networkLauncher == null)
            {
                networkLauncher = CWNetworkLauncher.instance;
            }

            failMessage.SetActive(false);

            // listen
            roomIDInputField.onValueChanged.AddListener(OnRoomIDValueChanged);
            joinButton.onClick.AddListener(OnJoinButtonPressed);
        }

        private void OnDisable()
        {
            roomIDInputField.onValueChanged.RemoveListener(OnRoomIDValueChanged);
            joinButton.onClick.RemoveListener(OnJoinButtonPressed);
        }

        public virtual void OnRoomIDValueChanged(string input)
        {
            if (input.Length > 0)
            {
                joinButton.interactable = true;
            }
            else
            {
                joinButton.interactable = false;
            }
        }

        public void OnJoinButtonPressed()
        {
            failMessage.SetActive(false);

            networkLauncher.AttemptJoinRoom(roomIDInputField.text);
        }

        public void OnJoinRoomFailed()
        {
            failMessage.SetActive(true);
        }
    }
}