﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CW
{
    public abstract class CWUser_Base : MonoBehaviourPunCallbacks, IPunObservable
    {
        public static CWUser_Base localUserInstance;
        protected CWNetworkManager networkManager;

        public Camera UICamera;

        public GameObject userNetworkObjects;

        protected virtual void Awake()
        {
            if (this.photonView.IsMine)
            {
                Debug.Log("CWUser: User is mine");
                localUserInstance = this;

                //userNetworkObjects.SetActive(false);

                string id = PhotonNetwork.NickName;
                this.photonView.RPC("RPC_SetUserID", RpcTarget.AllBuffered, id);
            }

            DontDestroyOnLoad(this.gameObject);

            // get the network manager
            networkManager = CWNetworkManager.instance;
        }

        /// <summary>
        /// This method is only called when the user was just created
        /// </summary>
        public abstract void InitializeLocalUser();

        protected virtual void ActivateUICamera(bool activate, Transform copyMe = null)
        {
            if (copyMe != null)
            {
                UICamera.transform.CopyGlobalTransform(copyMe, scl: false);
            }

            UICamera.gameObject.SetActive(activate);
        }

        [PunRPC]
        public void RPC_SetUserID(string userID)
        {
            Debug.Log("CWUser: SetUserID (" + userID + ")");
            gameObject.name = userID;
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            // DO SOMETHING
        }
    }
}
