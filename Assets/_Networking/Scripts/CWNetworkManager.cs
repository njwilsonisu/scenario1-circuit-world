﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRTK;

namespace CW
{
    public delegate void CWNetworkManagerEventHandler();

    public class CWNetworkManager : MonoBehaviourPunCallbacks, IOnEventCallback
    {
        public static CWNetworkManager instance;

        [HideInInspector]
        public byte SetApprenticeEventCode = 0;
        [HideInInspector]
        public byte SetInstructorEventCode = 1;
        [HideInInspector]
        public byte SetResearcherEventCode = 2;

        [HideInInspector]
        public byte SetCWParametersEventCode = 3;

        [Header("Local User Settings")]
        public CWUser_Base localUser;
        public string userID;
        public CWUserType userType;

        [Header("Circuit World Settings")]
        public string roomID;

        public bool isSetup { get; private set; } = false;

        public CWPlayerPlatform playerPlatform;
        public string participantID;

        [Header("Circuit World Users")]
        public CWUser_Base apprentice;
        public CWUser_Base instructor;
        public CWUser_Base researcher;


        [Header("User Prefabs")]
        public string userPrefabPath = "Prefabs/Users/";
        public GameObject apprenticeUserPrefab;
        public GameObject instructorUserPrefab;
        public GameObject researcherUserPrefab;

        [Header("FOR TESTING")]
        public bool testing = false;
        public CWPlayerPlatform testPlayerPlatform = CWPlayerPlatform.Desktop;

        private RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All, CachingOption = EventCaching.AddToRoomCache };
        
        // --------------------------------------------
        private bool quitting = false;
        // --------------------------------------------


        protected virtual void Awake()
        {
            // We need to make sure this is the only manager
            if (CWNetworkManager.instance != null && CWNetworkManager.instance != this)
            {
                Destroy(CWNetworkManager.instance.gameObject);
                CWNetworkManager.instance = this;
            }

            instance = this;

            //DontDestroyOnLoad(this.gameObject);

            Debug.Log("CWNetworkManager Created");
        }

        protected virtual void Start()
        {
            // If the local player hasnt been identified...
            if (CWUser_Base.localUserInstance == null)
            {
                // THIS IS THE TRUE SCENE START FOR THIS USER

                // get the users nickname
                userID = PhotonNetwork.NickName;

                // get the users type
                userType = GetUserType(userID);

                // create the new user
                CreateNewUser(userType);
            }
            // Otherwise...
            else
            {
                Debug.LogError("Here");
                localUser = CWUser_Base.localUserInstance;
            }
        }

        protected virtual string GetUserID(string n)
        {
            int end = n.IndexOf(" - ");

            string id = n.Substring(0, end);

            return id;
        }

        protected virtual CWUserType GetUserType(string n)
        {
            int openBracket = n.IndexOf("[") + 1;
            int closeBracket = n.IndexOf("]");

            string type = n.Substring(openBracket, closeBracket - openBracket);

            return (CWUserType)Enum.Parse(typeof(CWUserType), type);
        }

        public virtual void CreateNewUser(CWUserType userType)
        {
            GameObject newUser = null;

            // get the player prefab for the given type
            switch (userType)
            {
                case CWUserType.Apprentice:
                    newUser = apprenticeUserPrefab;
                    break;
                case CWUserType.Instructor:
                    newUser = instructorUserPrefab;
                    break;
                case CWUserType.Researcher:
                    newUser = researcherUserPrefab;
                    break;
            }

            string prefabName = userPrefabPath + newUser.name;

            // instantiate the prefab
            newUser = PhotonNetwork.Instantiate(prefabName, Vector3.zero, Quaternion.identity);

            // get the user script
            CWUser_Base user = newUser.GetComponent<CWUser_Base>();

            // set the static user type
            switch (userType)
            {
                case CWUserType.Apprentice:
                    SetApprentice(user);
                    break;
                case CWUserType.Instructor:
                    SetInstructor(user);
                    break;
                case CWUserType.Researcher:
                    SetResearcher(researcher);
                    break;
            }

            // FOR TESTING 
            if(testing && userType != CWUserType.Researcher)
            {
                SetCircuitWorldParameters(testPlayerPlatform, "TESTING");
            }

            // Initialize the user
            user.InitializeLocalUser();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);

            Debug.Log("Player has entered: " + newPlayer.UserId);

        }

        public override void OnPlayerLeftRoom(Player other)
        {
            if (PhotonNetwork.IsMasterClient)
            {

            }
        }

        public void DisconnectFromNetwork()
        {
            PhotonNetwork.Disconnect();
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);
            Debug.Log("CWNetworkManager: OnDisconnected");

            // load the launcher scene
            //SceneManager.LoadScene(0);
        }

        #region Network Events

        public void SetApprentice(CWUser_Base apprentice)
        {
            PhotonNetwork.RaiseEvent(SetApprenticeEventCode, apprentice, raiseEventOptions, SendOptions.SendReliable);
        }

        public void SetInstructor(CWUser_Base instructor)
        {
            PhotonNetwork.RaiseEvent(SetInstructorEventCode, instructor, raiseEventOptions, SendOptions.SendReliable);
        }

        public void SetResearcher(CWUser_Base researcher)
        {
            PhotonNetwork.RaiseEvent(SetResearcherEventCode, researcher, raiseEventOptions, SendOptions.SendReliable);
        }

        public void SetCircuitWorldParameters(CWPlayerPlatform playerPlatform, string participantID)
        {
            object[] data = new object[] { playerPlatform, participantID };
            PhotonNetwork.RaiseEvent(SetCWParametersEventCode, data, raiseEventOptions, SendOptions.SendReliable);
        }

        public void OnEvent(EventData photonEvent)
        {
            byte eventCode = photonEvent.Code;

            // Set Apprentice
            if (eventCode == SetApprenticeEventCode)
            {
                Debug.Log("CWNetworkManager: Setting Apprentice");
                researcher = (CWUser_Base)photonEvent.CustomData;
            }

            // Set Instructor
            if (eventCode == SetInstructorEventCode)
            {
                Debug.Log("CWNetworkManager: Setting Instructor");
                researcher = (CWUser_Base)photonEvent.CustomData;
            }

            // Set Researcher
            if (eventCode == SetResearcherEventCode)
            {
                Debug.Log("CWNetworkManager: Setting Researcher");
                researcher = (CWUser_Base)photonEvent.CustomData;
            }

            // Set Parameters
            if (eventCode == SetCWParametersEventCode)
            {
                Debug.Log("CWNetworkManager: Setting Parameters");
                object[] data = (object[])photonEvent.CustomData;
                playerPlatform = (CWPlayerPlatform)data[0];
                participantID = (string)data[1];
                isSetup = true;
            }
        }

        #endregion
    }
}