﻿namespace CW
{
    public enum CWUserType
    {
        Undefined = -1,
        Apprentice = 0,
        Instructor = 1,
        Researcher = 2
    }
}

