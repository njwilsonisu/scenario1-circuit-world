﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CW
{
    public class CWUser_Researcher : CWUser_Base
    {
        public StartCircuitWorldUI startCircuitWorldUI;

        public GameObject researcherControllerPrefab;

        public override void InitializeLocalUser()
        {
            // open the UI
            OpenStartCircuitWorldUI(true);
        }

        public void OpenStartCircuitWorldUI(bool open)
        {
            if (open)
            {
                startCircuitWorldUI.startCircuitWorldButton.onClick.AddListener(StartCircuitWorld);
            }
            else
            {
                startCircuitWorldUI.startCircuitWorldButton.onClick.RemoveListener(StartCircuitWorld);
            }
            startCircuitWorldUI.gameObject.SetActive(open);
            ActivateUICamera(open);
        }

        public void StartCircuitWorld()
        {
            // Get the UI info
            CWPlayerPlatform selectedPlayerPlatform = (CWPlayerPlatform)startCircuitWorldUI.playerPlatformButtonGroup.currentSelection; ;
            string participantID = startCircuitWorldUI.participantIDInputField.text;

            // set the parameters
            networkManager.SetCircuitWorldParameters(selectedPlayerPlatform, participantID);

            // close the UI
            OpenStartCircuitWorldUI(false);

            // enable the player controller
        }
    }
}