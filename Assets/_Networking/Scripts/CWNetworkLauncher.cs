﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CW
{
    public class CWNetworkLauncher : MonoBehaviourPunCallbacks
    {
        public static CWNetworkLauncher instance;

        public int circuitWorldSceneIndex = 1;

        public StartUI startUI;

        public JoinRoomUI joinRoomUI;
        public CreateRoomUI createRoomUI;

        [Header("User Info")]
        public string userID;
        public CWUserType userType;

        [Header("Network Info")]
        public string roomID;

        [Header("FOR TESTING")]
        public bool test = false;
        public string testUserID = "TEST USER";
        public CWUserType testUserType = CWUserType.Apprentice;

        public string testRoomID = "TEST ROOM";

        private bool isConnecting = false;


        public void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(instance.gameObject);
            }

            instance = this;

            PhotonNetwork.AutomaticallySyncScene = true;
        }

        public void Start()
        {
            if (test)
            {
                AttemptToConnect(testUserID, testUserType);
                return;
            }

            // close all the other menus
            CloseJoinRoomUI();
            CloseCreateRoomUI();
            OpenStartMenu();
        }

        public override void OnEnable()
        {
            base.OnEnable();
            PhotonNetwork.AddCallbackTarget(this);

        }

        public override void OnDisable()
        {
            base.OnDisable();
            PhotonNetwork.RemoveCallbackTarget(this);
        }

        public void OpenStartMenu()
        {
            startUI.gameObject.SetActive(true);
        }

        public void CloseStartMenu()
        {
            startUI.gameObject.SetActive(false);
        }

        public void OpenJoinRoomUI()
        {
            joinRoomUI.gameObject.SetActive(true);
            joinRoomUI.transform.parent.gameObject.SetActive(true);
        }

        public void CloseJoinRoomUI()
        {
            joinRoomUI.gameObject.SetActive(false);
            joinRoomUI.transform.parent.gameObject.SetActive(false);
        }

        public void OpenCreateRoomUI()
        {
            createRoomUI.gameObject.SetActive(true);
            createRoomUI.transform.parent.gameObject.SetActive(true);
        }

        public void CloseCreateRoomUI()
        {
            createRoomUI.gameObject.SetActive(false);
            createRoomUI.transform.parent.gameObject.SetActive(false);
        }

        public void FailedToConnect(string reason)
        {
            startUI.FailedToConnect();
        }

        public void AttemptToConnect(string userID, CWUserType userType)
        {
            Debug.Log("CWNetworkLauncher: Attempting User Login");
            this.userID = userID;
            this.userType = userType;

            // set the users nickname
            SetUserNickname(userID, userType);

            // then connect to the network
            ConnectToNetwork();
        }


        public void SetUserNickname(string userID, CWUserType userType)
        {
            string n = userID + " - [" + userType.ToString() + "]";
            Debug.Log("CWNetworkLauncher: Set User Nickname to " + n);
            PhotonNetwork.NickName = n;
        }

        public void ConnectToNetwork()
        {
            Debug.Log("CWNetworkLauncher: ConnectToNetwork");

            isConnecting = true;

            PhotonNetwork.ConnectUsingSettings();
        }

        // Once we have connected to the network...
        public override void OnConnectedToMaster()
        {
            if (test)
            {
                if (isConnecting)
                {
                    AttemptCreateRoom(testRoomID);
                    isConnecting = false;
                }
                return;
            }

            if (isConnecting)
            {
                CloseStartMenu();

                switch (userType)
                {
                    case CWUserType.Apprentice:
                    case CWUserType.Instructor:
                        OpenJoinRoomUI();
                        break;
                    case CWUserType.Researcher:
                        OpenCreateRoomUI();
                        break;
                }

                isConnecting = false;
            }
            
            Debug.Log("CWNetworkLauncher: OnConnectedToMaster");
        }

        public void DisconnectFromNetwork()
        {
            Debug.Log("CWNetworkLauncher: DisconnectFromNetwork");

            PhotonNetwork.Disconnect();
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.Log("CWNetworkLauncher: OnDisconnected");

            isConnecting = false;
        }


        public void AttemptCreateRoom(string roomID)
        {
            Debug.Log("CWNetworkLauncher: AttemptCreateRoom");

            this.roomID = roomID;

            RoomOptions roomOptions = new RoomOptions() { IsVisible = false, MaxPlayers = 3 };

            PhotonNetwork.CreateRoom(roomID, roomOptions);
        }

        public override void OnCreatedRoom()
        {
            Debug.Log("CWNetworkLauncher: OnCreatedRoom");

            base.OnCreatedRoom();
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            Debug.LogError("CWNetworkLauncher: OnCreateRoomFailed - " + message);

            base.OnCreateRoomFailed(returnCode, message);

            createRoomUI.OnCreateRoomFailed();
        }

        public void AttemptJoinRoom(string roomID)
        {
            Debug.Log("CWNetworkLauncher: AttemptJoinRoom");

            this.roomID = roomID;

            PhotonNetwork.JoinRoom(roomID);
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("CWNetworkLauncher: OnJoinedRoom");

            base.OnJoinedRoom();

            LoadCircuitWorld();
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            Debug.LogError("CWNetworkLauncher: OnJoinRoomFailed - " + message);

            base.OnJoinRoomFailed(returnCode, message);

            joinRoomUI.OnJoinRoomFailed();
        }

        public void LoadCircuitWorld()
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                Debug.Log("CWNetworkLauncher: Loading CircuitWorld");
                PhotonNetwork.LoadLevel(circuitWorldSceneIndex);
            }
        }

    }
}