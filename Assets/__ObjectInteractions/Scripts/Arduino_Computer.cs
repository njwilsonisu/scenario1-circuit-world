﻿using SharpCircuit;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arduino_Computer : MonoBehaviour
{
    public Arduino_Code_Base code;

    public List<Arduino_AnalogInput> inputTerminals = new List<Arduino_AnalogInput>();
    
    public List<Arduino_DigitalOutput> outputTerminals = new List<Arduino_DigitalOutput>();

    // the analog values
    public float[] inputs;

    // digital values
    protected int[] digital_inputs;

    public float[] outputs;
    protected int[] digital_outputs;

    protected virtual void Awake()
    {
        if(inputTerminals == null || inputTerminals.Count == 0)
        {
            inputTerminals.AddRange(transform.GetComponentsInChildren<Arduino_AnalogInput>());
        }

        if (outputTerminals == null || outputTerminals.Count == 0)
        {
            outputTerminals.AddRange(transform.GetComponentsInChildren<Arduino_DigitalOutput>());
        }

        if(code == null)
        {
            code = gameObject.GetComponent<Arduino_Code_Base>();
        }
    }

    protected virtual void Update()
    {
        // get the inputs
        inputs = ReadInputs();

        // convert them to digital
        digital_inputs = AnalogToDigital(inputs);

        // pass through the code and get the digital ouputs
        digital_outputs = code.ProcessInputs(digital_inputs);

        // convert to analog
        outputs = DigitalToAnalog(digital_outputs);

        // Set the outputs
        WriteOutputs(outputs);
    }

    protected virtual float[] ReadInputs()
    {
        // create an array to store the input values
        float[] temp = new float[inputTerminals.Count];

        for (int i = 0; i < inputTerminals.Count; i++)
        {
            temp[i] = (float)inputTerminals[i].inputValue;
        }
        return temp;
    }

    protected virtual void WriteOutputs(float[] outputs)
    {
        for (int i = 0; i < outputTerminals.Count; i++)
        {
            outputTerminals[i].UpdateOutputVoltage(outputs[i]);
        }
    }

    protected virtual int[] AnalogToDigital(float[] analog)
    {
        // initialize array
        int[] digital = new int[analog.Length];

        for (int i = 0; i < analog.Length; i++)
        {
            // get the float value
            float f = Mathf.Lerp(0, 1023, analog[i] / 5f);

            digital[i] = (int)f;
        }

        return digital;
    }

    protected virtual float[] DigitalToAnalog(int[] digital)
    {
        // initialize array
        float[] analog = new float[digital.Length];

        for (int i = 0; i < digital.Length; i++)
        {
            // get the float value
            float f = (float)digital[i];
            analog[i] = Mathf.Lerp(0f, 5f, f / 255f);
        }

        return analog;
    }


}
