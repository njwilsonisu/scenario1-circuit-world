﻿using SharpCircuit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arduino_5V : ElectricalComponent
{
    // This is the 5V DC voltage source that is attached to the arduino board

    public override CircuitElement GetCircuitElement()
    {
        // Defaults to 5V
        VoltageInput v = new VoltageInput(Voltage.WaveType.DC);
        return v;
    }
}
