﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class StripGrid
{
    [HideInInspector]
    public List<List<Vector3>> strips = new List<List<Vector3>>();
}

[System.Serializable]
public class TerminalStripGrid : StripGrid
{
    public Transform StartingTerminal;
    public int NumberOfRows;
    public float dRows;
    public int NumberOfColumns;
    public float dColumns;
}

[System.Serializable]
public class BusStripGrid : StripGrid
{
    public Transform StartingTerminal;
    public float dColumns;
    public int TerminalsPerGroup;
    public float dTerminals;
    public int NumberOfGoups;
    public float dGroups;
}

public class BreadboardMaker : MonoBehaviour
{
    // This class is used to create the necessary components that make up a breadboard using strips of terminal plugs

    // Each breadboard has a # terminal strips (run horizontally) and a # of bus strips (run vertically)

    public GameObject terminalPlugPrefab;

    public List<TerminalStripGrid> _terminalStripGrids = new List<TerminalStripGrid>();
    public List<BusStripGrid> _busStripGrids = new List<BusStripGrid>();

    public bool showTerminalLocations = true;

    protected virtual void OnEnable()
    {
        //RegenerateStrips();
    }

    public virtual void RegenerateStrips()
    {
        RemoveAllStrips();

        // Create the terminal strip grids
        for (int i = 0; i < _terminalStripGrids.Count; i++)
        {
            // The holder object is the starting terminal transform

            // Rename the object
            _terminalStripGrids[i].StartingTerminal.gameObject.name = "TerminalStripGrid " + (i + 1).ToString();

            for (int j = 0; j < _terminalStripGrids[i].NumberOfRows; j++)
            {
                // create a new gameobject for the strip
                GameObject ts = new GameObject("TerminalStrip " + (j + 1).ToString());

                // set the parent to the grid
                ts.transform.parent = _terminalStripGrids[i].StartingTerminal;

                // Store the strips position
                Vector3 p = -transform.forward * _terminalStripGrids[i].dRows * j;

                // Set the transform
                ts.transform.localPosition = p;

                // add the strip script to it
                BreadboardStrip strip = ts.AddComponent<BreadboardStrip>();

                // ------------------------------
                strip.TerminalPlugPrefab = terminalPlugPrefab;
                // ------------------------------

                // Initialize the strip
                InitializeStrip(strip, _terminalStripGrids[i].NumberOfColumns, _terminalStripGrids[i].dColumns);
            }
        }

        // Create the bus strip grids
        for (int i = 0; i < _busStripGrids.Count; i++)
        {
            _busStripGrids[i].StartingTerminal.gameObject.name = "BusStripGrid " + (i + 1).ToString();
            //_busStripGrids[i].StartingTerminal.localEulerAngles = new Vector3(0, 90f, 0);

            for (int j = 0; j < 2; j++)
            {
                // create a new gameobject for the strip
                GameObject ts = new GameObject("BusStrip " + (j + 1).ToString());

                // set the parent to the grid
                ts.transform.parent = _busStripGrids[i].StartingTerminal;
                ts.transform.localEulerAngles = new Vector3(0, 90f, 0);

                // Store the strips position
                Vector3 p = transform.right * _busStripGrids[i].dColumns * j;

                // Set the transform
                ts.transform.localPosition = p;

                // add the strip script to it
                BreadboardStrip strip = ts.AddComponent<BreadboardStrip>();

                // ------------------------------
                strip.TerminalPlugPrefab = terminalPlugPrefab;
                // ------------------------------

                // Initialize the strip
                InitializeGroupedStrip(strip, 5, _busStripGrids[i].dTerminals, _busStripGrids[i].NumberOfGoups, _busStripGrids[i].dGroups);
            }
        }
    }

    public virtual void RemoveAllStrips()
    {
        List<BreadboardStrip> strips = new List<BreadboardStrip>();
        strips.AddRange(GetComponentsInChildren<BreadboardStrip>());

        foreach (BreadboardStrip strip in strips)
        {
            DestroyImmediate(strip.gameObject);
        }
    }

    public virtual void RemoveAllTerminals()
    {
        List<Plug_Terminal> strips = new List<Plug_Terminal>();
        strips.AddRange(GetComponentsInChildren<Plug_Terminal>());

        foreach (Plug_Terminal strip in strips)
        {
            DestroyImmediate(strip.gameObject);
        }
    }

    /// <summary>
    /// Creates a given number of terminals, each seperated by the given distance in the x axis
    /// </summary>
    /// <param name="nTerminals"></param>
    /// <param name="distance"></param>
    public virtual void InitializeStrip(BreadboardStrip strip, int nTerminals, float distance)
    {
        for (int i = 0; i < nTerminals; i++)
        {
            // create a new terminal plug
            //GameObject g = Instantiate(terminalPlugPrefab, strip.transform);
            GameObject g = (GameObject)PrefabUtility.InstantiatePrefab(terminalPlugPrefab, strip.transform);
            g.name = "TerminalPlug " + i.ToString();

            // set the local position
            g.transform.localPosition = strip.transform.right * distance * i;
            // add the new terminal to the strips list
            strip.terminals.Add(g.GetComponent<Plug_Terminal>());
        }
    }

    /// <summary>
    /// Creates groups of terminals each seperated by the given distance in the x axis
    /// </summary>
    /// <param name="distanceBetweenGroups"></param> the distance between the last terminal of the previous group and the first terminal of the next
    public virtual void InitializeGroupedStrip(BreadboardStrip strip, int terminalsPerGroup, float distanceBetweenTerminals, int numberOfGroups, float distanceBetweenGroups)
    {
        Vector3 tpos = Vector3.zero;
        for (int i = 0; i < numberOfGroups; i++)
        {
            for (int j = 0; j < terminalsPerGroup; j++)
            {
                //GameObject g = Instantiate(terminalPlugPrefab, strip.transform);
                GameObject g = (GameObject)PrefabUtility.InstantiatePrefab(terminalPlugPrefab, strip.transform);
                g.name = "TerminalPlug " + i.ToString() + "." + j.ToString();
                g.transform.localPosition = tpos;
                strip.terminals.Add(g.GetComponent<Plug_Terminal>());

                tpos += Vector3.right * distanceBetweenTerminals;
            }
            tpos += Vector3.right * (distanceBetweenGroups - distanceBetweenTerminals);
        }
    }

    protected virtual void OnDrawGizmos()
    {
        if (showTerminalLocations)
        {
            Gizmos.color = Color.blue;
            foreach (TerminalStripGrid tsg in _terminalStripGrids)
            {
                for (int i = 0; i < tsg.NumberOfRows; i++)
                {
                    for (int j = 0; j < tsg.NumberOfColumns; j++)
                    {
                        Vector3 p = transform.TransformPoint(tsg.StartingTerminal.localPosition
                            - transform.forward * tsg.dRows * i
                            + transform.right * tsg.dColumns * j);
                        Gizmos.DrawSphere(p, 0.005f);
                    }
                }
            }

            foreach (BusStripGrid bsg in _busStripGrids)
            {
                for (int c = 0; c < 2; c++)
                {
                    for (int i = 0; i < bsg.NumberOfGoups; i++)
                    {
                        for (int j = 0; j < bsg.TerminalsPerGroup; j++)
                        {
                            Vector3 p = transform.TransformPoint(bsg.StartingTerminal.localPosition
                                + transform.right * bsg.dColumns * c
                                - transform.forward * bsg.dGroups * i - transform.forward * (bsg.TerminalsPerGroup - 1) * bsg.dTerminals * i
                                - transform.forward * bsg.dTerminals * j
                                );

                            Gizmos.DrawSphere(p, 0.005f);
                        }
                    }
                }
            }

            //showTerminalLocations = false;
        }

    }
}
