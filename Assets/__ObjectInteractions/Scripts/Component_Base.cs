﻿using SharpCircuit;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using VRTK;

[ExecuteInEditMode]
public abstract class Component_Base : ElectricalComponent
{
    [Header("Component Wires")]
    public GameObject wirePrefab;

    /// <summary>
    /// The wires that are used to plug in the component
    /// </summary>
    public Wire_Component[] connectorWires;

    /// <summary>
    /// The plugs that are used to connect the component
    /// </summary>
    public Plug_Wire[] connectorPlugs;

    public float midpointMultiplier = 0.5f;

    // the interactable object script for this object
    protected VRTK_InteractableObject interactableObject;

    protected override void OnEnable()
    {
        // make sure we have the interactable 
        if (interactableObject == null)
        {
            interactableObject = gameObject.GetComponent<VRTK_InteractableObject>();

            if (interactableObject == null)
            {
                interactableObject = gameObject.AddComponent<VRTK_InteractableObject>();
            }
        }

        // subscribe to the interactable object events
        interactableObject.InteractableObjectGrabbed += OnComponentGrabbed;
        interactableObject.InteractableObjectSnappedToDropZone += OnComponentSnapped;

        // regenerate the wires if need be
        if (connectorWires == null || connectorWires.Length == 0 || connectorPlugs == null || connectorPlugs.Length == 0)
        {
            RegenerateWires();
        }

        // --- Listening to the connector plugs instead of the component plug ---
        foreach (Plug_Base plug in connectorPlugs)
        {
            if (plug != null)
            {
                plug.ListenForPlugConnected(OnPlugConnected, true);
                plug.ListenForPlugDisconnected(OnPlugDisconnected, true);
            }
        }
        // ---------------------------------------------------------------------

        // if it was connected but its not anymore
        if (isConnected && !AllPlugsConnected())
        {
            OnComponentDisconnected();
        }
        // if it wasnt connect but now it is
        else if (!isConnected && AllPlugsConnected())
        {
            OnComponentConnected();
        }
    }

    protected override void OnDisable()
    {
        foreach (Plug_Base plug in connectorPlugs)
        {
            if (plug != null)
            {
                plug.ListenForPlugConnected(OnPlugConnected, false);

                plug.ListenForPlugDisconnected(OnPlugDisconnected, false);
            }
        }

        if (interactableObject != null)
        {
            interactableObject.InteractableObjectGrabbed -= OnComponentGrabbed;

            interactableObject.InteractableObjectSnappedToDropZone -= OnComponentSnapped;
        }
    }

    public virtual void OnComponentGrabbed(object sender, InteractableObjectEventArgs e)
    {
        foreach (Wire_Component wire in connectorWires)
        {
            wire.follow = true;
        }
    }

    public virtual void OnComponentSnapped(object sender, InteractableObjectEventArgs e)
    {
        // The component has been dropped in the circuit builder workstation

        foreach (Wire_Component wire in connectorWires)
        {
            // Activate all the wires
            wire.Activate();
        }

    }

    protected override bool AllPlugsConnected()
    {
        return AllWiresConnected();
    }

    protected virtual bool AllWiresConnected()
    {
        foreach (Wire_Component wire in connectorWires)
        {
            if (!wire.isConnected)
            {
                return false;
            }
        }

        return true;
    }

    protected override void OnComponentConnected()
    {
        base.OnComponentConnected();

        // disable the interactable object
        if(interactableObject != null)
        {
            interactableObject.disableWhenIdle = false;
            interactableObject.enabled = false;
        }

        // ----------------------------------------------------------------------

        // Stop listening for the object grab

        // Listen for the grab button held while touching to disconnect

        // ----------------------------------------------------------------------

        // reset the component to be at the midpoint
        ResetComponentPosition();
    }

    protected override void OnComponentDisconnected()
    {
        base.OnComponentDisconnected();



        // enable the interactable object
        if (interactableObject != null)
        {
            interactableObject.disableWhenIdle = true;
            interactableObject.enabled = true;
        }
    }

    public virtual void ResetComponentPosition()
    {
        // get a list of the wire plug transforms
        List<Transform> connectorPlugTransforms = connectorWires.Select(w => w.GetPlug<Plug_Wire>(1).transform).ToList();

        // get all the global positions
        List<Vector3> connectorPlugPositions = connectorPlugTransforms.Select(tf => tf.position).ToList();

        float scaleMultiplier = transform.lossyScale.magnitude;
        //Debug.LogError(s);

        // get the midpoint
        Vector3 mid = connectorPlugTransforms.RaisedMidpoint(midpointMultiplier * scaleMultiplier);

        // position the component at the midpoint
        transform.position = mid;

        // get the wire plugs with the greatests distance between them
        Transform[] maxConnectorPlugs = connectorPlugTransforms.MaximumDistance();

        // get the corresponding component plugs
        Transform[] maxComponentPlugs = new Transform[maxConnectorPlugs.Length];

        for (int i = 0; i < maxConnectorPlugs.Length; i++)
        {
            // get the index of the maximum connector plugs
            int index = Array.IndexOf(connectorPlugs, maxConnectorPlugs[i].GetComponent<Plug_Wire>());
            maxComponentPlugs[i] = plugs[index].transform;
        }

        // direction from wireplug -> wireplug
        Vector3 d1 = Vector3.Normalize(maxConnectorPlugs[1].position - maxConnectorPlugs[0].position);
        // direction from componentplug -> componentplug
        Vector3 d2 = transform.InverseTransformDirection(Vector3.Normalize(maxComponentPlugs[1].position - maxComponentPlugs[0].position));

        // rotation difference from forward to d1
        var r1 = Quaternion.LookRotation(d1, Vector3.up);
        // rotation difference from forward to d2
        var r2 = Quaternion.LookRotation(d2, Vector3.up);

        // rotate the object so the forward faces d2 --> the recorrect for the rotation offset
        transform.rotation = r1 * Quaternion.Inverse(r2);

        // reset all of the plug positions
        for (int i = 0; i < connectorPlugTransforms.Count; i++)
        {
            connectorPlugTransforms[i].position = connectorPlugPositions[i];
        }

        // make sure the wires follow
        foreach (Wire_Component wire in connectorWires)
        {
            wire.UpdateSplineNodes();
        }
    }

    public virtual void RegenerateWires()
    {
        // we are going to need the wire prefab
        if (wirePrefab == null)
        {
            Debug.LogError("Component Wire Prefab not assigned");
            return;
        }

        connectorWires = GetLeadWires();
        connectorPlugs = new Plug_Wire[NumberOfLeads()];

        // for each plug...
        for (int i = 0; i < plugs.Length; i++)
        {
            if (plugs[i] == null)
            {
                Debug.LogError("Plug not assigned");
                continue;
            }

            // get the component plug
            Plug_Component plug = GetPlug<Plug_Component>(i);

            // make sure the plug is disconnected
            plug.DisconnectFromPlug();

            // rename the corresponding wire
            connectorWires[i].gameObject.name = leads[i].name + " Wire";

            // forcefully connect the wire to the new plug
            connectorWires[i].GetPlug<Plug_Wire>(0).ConnectToPlug(plug, true);

            // get the wires other plug
            Plug_Wire componentPlug = connectorWires[i].GetPlug<Plug_Wire>(1);

            // save the plug
            connectorPlugs[i] = componentPlug;

            // Set the connector plug to the default position
            componentPlug.gameObject.SetActive(false);
            componentPlug.transform.localPosition = new Vector3(0, 0.05f, 0);
            componentPlug.transform.up = -plug.transform.up;
            componentPlug.gameObject.SetActive(true);

            if (componentPlug.isConnected && componentPlug.connectedPlug != null)
            {
                componentPlug.ConnectToPlug(componentPlug.connectedPlug, true);
            }

            connectorWires[i].UpdateSplineNodes();
        }

        // make sure all the valid plugs are given their lead references
        ResetLeadReferences();
    }

    public virtual void DestroyAllWires()
    {
        // Create a new list of wires
        Wire_Component[] allwires;
        allwires = GetComponentsInChildren<Wire_Component>();

        for (int i = 0; i < allwires.Length; i++)
        {
            DestroyImmediate(allwires[i].gameObject);
        }
    }

    public virtual Wire_Component[] GetLeadWires()
    {
        // Create a new array of wires
        Wire_Component[] newWires = new Wire_Component[NumberOfLeads()];

        // get all the children wires
        Wire_Component[] childWires = GetComponentsInChildren<Wire_Component>();

        // Delete excess wires if needed
        if (childWires.Length > NumberOfLeads())
        {
            // how many excess wires
            int n = childWires.Length - NumberOfLeads();
            for (int i = 0; i < n; i++)
            {
                int index = childWires.Length - 1;
                DestroyImmediate(childWires[index - i].gameObject);
            }
        }

        for (int i = 0; i < newWires.Length; i++)
        {
            // placeholder
            Wire_Component w = null;

            // try and get the old wire reference
            if(connectorWires != null && connectorWires.Length > i)
            {
                w = connectorWires[i];
            }

            // if that wasnt available...
            if(w == null)
            {
                // try getting the child wire
                if (childWires.Length > i)
                {
                    w = childWires[i];
                }
            }

            // if that wasnt available...
            if (w == null)
            {
                // create a new wire prefab
                GameObject g = (GameObject)PrefabUtility.InstantiatePrefab(wirePrefab);
                w = g.GetComponent<Wire_Component>();
            }

            newWires[i] = w;
        }

        return newWires;
    }

    // Only used to draw the lines in the scene view
    protected virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        if (connectorWires != null && connectorWires.Length > 0)
        {
            List<Transform> tfs = connectorWires.Select(w => w.GetPlug<Plug_Wire>(1).transform).ToList();
            float size = transform.lossyScale.magnitude;

            Gizmos.DrawSphere(tfs.RaisedMidpoint(midpointMultiplier * size), 0.01f * size);
        }
    }

    public abstract override CircuitElement GetCircuitElement();
}
