﻿using SharpCircuit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arduino_AnalogInput : ElectricalComponent
{
    // COMPUTER INPUT -> CIRCUIT INPUT OUTPUT
    
    public float inputValue = 0f;
    protected float previousInputValue = 0f;
    
    protected Output element;

    public override CircuitElement GetCircuitElement()
    {
        element = new Output();
        return element;
    }

    protected virtual void Update()
    {
        if(element != null)
        {
            inputValue = (float)element.getLeadVoltage(0);
        }

        if(inputValue != previousInputValue)
        {
            needsUpdating = true;

            previousInputValue = inputValue;
        }
        //else
        //{
        //    needsUpdating = false;
        //}
    }
}
