﻿using SharpCircuit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Component_Resistor : Component_Base
{
    [Header("Resistor Component")]

    public float resistance = 1000f;

    public override CircuitElement GetCircuitElement()
    {
        Resistor r = new Resistor((double)resistance);
        return r;
    }

}
