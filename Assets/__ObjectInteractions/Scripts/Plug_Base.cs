﻿using SharpCircuit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public struct PlugEventArgs
{
    public Plug_Base sender;
    public Plug_Base connection;
}

public delegate void PlugEventHandler(PlugEventArgs e);

public class Plug_Base : MonoBehaviour
{
    // Each plug will serve as a "lead" for an electrical component

    public bool isConnected = false;

    protected bool wasForced = false;

    public Plug_Base connectedPlug;

    public event PlugEventHandler PlugConnected;
    public event PlugEventHandler PlugDisconnected;

    protected int attemptedDisconnects = 0;

    public virtual bool IsStationary()
    {
        // Override me
        return false;
    }

    private ElectricalLead _lead;

    public ElectricalLead lead
    {
        get
        {
            if (_lead == null)
            {
                _lead = GetLeadReferenceFromParent();
            }
            return _lead;
        }
        set
        {
            gameObject.name = value.name;
            _lead = value;
        }
    }

    protected virtual ElectricalLead GetLeadReferenceFromParent()
    {
        ElectricalLead l = null;
        ElectricalComponent parent = transform.parent.GetComponent<ElectricalComponent>();
        if (parent != null)
        {
            l = parent.GetPlugLead(this);
            //Debug.LogError("Lead reference: " + l + " from parent: " + parent.name);
        }
        return l;
    }



    public virtual void ConnectToPlug(GameObject plug, bool force = false)
    {
        Plug_Base p = plug.GetComponent<Plug_Base>();

        if (p != null)
        {
            ConnectToPlug(p, force);
        }
    }

    public virtual void ConnectToPlug(Plug_Base plug, bool force = false)
    {
        if (plug != null)
        {
            if (force)
            {
                AttemptForceConnect(plug);
            }

            // if the plug is not already connected...
            if (!isConnected)
            {
                //Debug.LogError("Connect to " + this.name + ": " + plug.name);
                isConnected = true;

                // add the given plug
                connectedPlug = plug;

                // if the other plug is not connected to this plug...
                if (!plug.isConnected)
                {
                    // connect it
                    plug.ConnectToPlug(this);
                }

                // connect the leads
                if (lead != null)
                {
                    lead.AddConnectedLead(plug.lead);
                }

                // Event
                if (PlugConnected != null)
                {
                    PlugEventArgs e = new PlugEventArgs { sender = this, connection = plug };
                    PlugConnected(e);
                }
            }
        }

    }

    public virtual void DisconnectFromPlug()
    {
        // If the given plug is currently connected...
        if (isConnected)
        {
            isConnected = false;

            Plug_Base plug = connectedPlug;

            connectedPlug = null;

            if (plug != null)
            {
                // disconnect the leads
                if (lead != null)
                    lead.RemoveConnectedLead(plug.lead);

                // deliver the message to the other plug if need be
                if (plug.isConnected)
                {
                    plug.DisconnectFromPlug();
                }
            }

            // Event
            if (PlugDisconnected != null)
            {
                PlugEventArgs e = new PlugEventArgs { sender = this, connection = plug };
                PlugDisconnected(e);
            }

        }
    }

    public virtual void AttemptForceConnect(Plug_Base plug)
    {
        // if this plug can move
        if (!IsStationary())
        {
            wasForced = true;

            // align my location to the plugs
            transform.CopyGlobalTransform(plug.transform, scl: false);
        }
        // if I cant move but the other can
        else if (!plug.IsStationary())
        {
            plug.AttemptForceConnect(this);
        }
    }

    public virtual void ListenForPlugConnected(PlugEventHandler p, bool listen)
    {
        if (listen)
        {
            PlugConnected += p;
        }
        else
        {
            PlugConnected -= p;
        }
    }

    public virtual void ListenForPlugDisconnected(PlugEventHandler p, bool listen)
    {
        if (listen)
        {
            PlugDisconnected += p;
        }
        else
        {
            PlugDisconnected -= p;
        }
    }

    protected virtual void OnEnable()
    {
        // if this plug was connected but now it isnt
        if (isConnected && connectedPlug == null)
        {
            DisconnectFromPlug();
        }
        // if this plug was not connected but now it is
        else if (!isConnected && connectedPlug != null)
        {
            ConnectToPlug(connectedPlug, wasForced);
        }
    }

    protected virtual void OnDisable()
    {

    }

    protected virtual void OnDestroy()
    {
        if (isConnected)
        {
            DisconnectFromPlug();
        }
    }

    // ------------------------------------ Multiple Connected Plugs ------------------------------------

    // the plugs this plug is plugged into ... plug
    //public List<Plug_Base> connectedPlugs = new List<Plug_Base>();

    //public bool IsConnected
    //{
    //    get
    //    {
    //        // if there are connected plugs...
    //        if(connectedPlugs.Count > 0)
    //        {
    //            // and at least one of them is not null...
    //            foreach(Plug_Base plug in connectedPlugs)
    //            {
    //                if (!(plug is null))
    //                {
    //                    return true;
    //                }
    //            }
    //        }
    //        return  false;
    //    }
    //}

    //public event PlugEventHandler PlugConnected;
    //public event PlugEventHandler PlugDisconnected;

    //protected int attemptedDisconnects = 0;

    //public virtual void ConnectToPlug(GameObject plug, bool force = false)
    //{
    //    Plug_Base p = plug.GetComponent<Plug_Base>();

    //    if (p != null)
    //    {
    //        ConnectToPlug(p, force);
    //    }
    //}

    //public virtual void ConnectToPlug(Plug_Base plug, bool force = false)
    //{
    //    if (plug is null)
    //    {
    //        return;
    //    }

    //    // if the plug is not already connected...
    //    if (!IsConnectedToPlug(plug))
    //    {
    //        // add the given plug
    //        AddConnectedPlug(plug);

    //        // if the other plug is not connected to this plug...
    //        if (!plug.IsConnectedToPlug(this))
    //        {
    //            // connect it
    //            plug.ConnectToPlug(this);
    //        }

    //        if (force)
    //        {
    //            // align our locations
    //            transform.CopyGlobalTransform(plug.transform, scl: false);
    //        }

    //        // Event
    //        if (PlugConnected != null)
    //        {
    //            PlugEventArgs e = new PlugEventArgs { sender = this, connection = plug };
    //            PlugConnected(e);
    //        }
    //    }

    //}

    //public virtual void DisconnectFromPlug(Plug_Base plug)
    //{
    //    if (plug is null)
    //    {
    //        return;
    //    }

    //    // If the given plug is currently connected...
    //    if (IsConnectedToPlug(plug))
    //    {
    //        RemoveConnectedPlug(plug);

    //        // deliver the message to the other plug if need be
    //        if (plug.IsConnectedToPlug(this))
    //        {
    //            plug.DisconnectFromPlug(this);
    //        }

    //        // Event
    //        if (PlugDisconnected != null)
    //        {
    //            PlugEventArgs e = new PlugEventArgs { sender = this, connection = plug };
    //            PlugDisconnected(e);
    //        }
    //    }
    //}

    //public virtual bool IsConnectedToPlug(Plug_Base plug)
    //{
    //    if (plug is null)
    //    {
    //        return false;
    //    }

    //    return connectedPlugs.Contains(plug);
    //}

    //public virtual void AddConnectedPlug(Plug_Base plug)
    //{
    //    if (plug is null)
    //    {
    //        return;
    //    }

    //    // doesnt add duplicates
    //    VRTK_SharedMethods.AddListValue(connectedPlugs, plug, true);
    //}

    //public virtual void RemoveConnectedPlug(Plug_Base plug)
    //{
    //    if (plug is null)
    //    {
    //        return;
    //    }

    //    if (IsConnectedToPlug(plug))
    //    {
    //        connectedPlugs.Remove(plug);
    //    }
    //}

    //public virtual void DisconnectFromAllPlugs()
    //{
    //    foreach(Plug_Base plug in connectedPlugs)
    //    {
    //        DisconnectFromPlug(plug);
    //    }
    //}

    //protected virtual void OnDisable()
    //{
    //    if (IsConnected)
    //    {
    //        DisconnectFromAllPlugs();
    //    }
    //}

    //protected virtual void OnDestroy()
    //{
    //    if (IsConnected)
    //    {
    //        DisconnectFromAllPlugs();
    //    }
    //}

}
