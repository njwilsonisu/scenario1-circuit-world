﻿using SharpCircuit;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Component_RGB : Component_Base
{
    [Header ("RGB Component")]

    public LEDElm element;

    public GameObject lightBulb;
    public Light pointLight;
    
    public Vector3 currentRGB;
    public Vector3 previousRGB;

    protected Color lightColor;
    protected Renderer lBRenderer;
    protected MaterialPropertyBlock mpb;
    

    public override CircuitElement GetCircuitElement()
    {
        element = new LEDElm();
        return element;
    }

    protected override void Awake()
    {
        base.Awake();

        if (lBRenderer == null && lightBulb != null)
        {
            lBRenderer = lightBulb.GetComponent<Renderer>();
            mpb = new MaterialPropertyBlock();
        }
    }

    protected virtual void Update()
    {
        if (element != null)
        {
            // Get the RGB values from the element
            currentRGB = GetRGB();

            if(previousRGB != currentRGB)
            {
                needsUpdating = true;

                SetRGB(currentRGB);

                previousRGB = currentRGB;
            }

            //UpdateLED();
        }
    }

    protected virtual Vector3 GetRGB()
    {
        float r = (float)element.getLeadVoltage(0);
        float g = (float)element.getLeadVoltage(2);
        float b = (float)element.getLeadVoltage(3);

        return new Vector3(r, g, b);
    }

    protected virtual void SetRGB(Vector3 rgb)
    {
        float[] t = new float[] { rgb[0], rgb[1], rgb[2] };

        float max = t.Max();

        // if the max value is 0 then force the color to not be black
        if (max == 0)
        {
            for (int i = 0; i < 3; i++)
            {
                rgb[i] = 0.02f;
            }
            max = 0.1f;
        }

        // adjust the values
        for (int i = 0; i < 3; i++)
        {
            rgb[i] = Mathf.Lerp(0f, 1f, rgb[i] / max);
        }

        // get the brightness
        var brightness = Mathf.Lerp(0, 2f, max / 5f);

        // update the color
        lightColor = new Color(rgb[0], rgb[1], rgb[2]);

        // set the material
        mpb.SetColor("_EmissionColor", lightColor * 1f);

        lBRenderer.SetPropertyBlock(mpb);

        // set the light
        pointLight.color = lightColor;
        pointLight.intensity = brightness;
    }

    protected virtual void UpdateLED()
    {
        float r = (float)element.getLeadVoltage(0);
        float g = (float)element.getLeadVoltage(2);
        float b = (float)element.getLeadVoltage(3);

        currentRGB = new Vector3(r, g, b);

        float[] t = new float[] { currentRGB[0], currentRGB[1], currentRGB[2] };

        float max = t.Max();

        // if the max value is 0 then force the color to not be black
        if(max == 0)
        {
            for (int i = 0; i < 3; i++)
            {
                currentRGB[i] = 0.02f;
            }
            max = 0.1f;
        }

        // adjust the values
        for (int i = 0; i < 3; i++)
        {
            currentRGB[i] = Mathf.Lerp(0f, 1f, currentRGB[i] / max);
        }

        // get the brightness
        var brightness = Mathf.Lerp(0, 2f, max / 5f);

        // update the color
        lightColor = new Color(currentRGB[0], currentRGB[1], currentRGB[2]);
        
        // set the material
        mpb.SetColor("_EmissionColor", lightColor * 1f);

        lBRenderer.SetPropertyBlock(mpb);

        // set the light
        pointLight.color = lightColor;
        pointLight.intensity = brightness;
    }
}
