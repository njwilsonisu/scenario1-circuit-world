﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arduino_Code_Potentiometer : Arduino_Code_Base
{
    public override int[] ProcessInputs(int[] inputInfo)
    {
        // get the value from sensor 0
        int i0 = inputInfo[0];

        float h = Mathf.Lerp(0f, 1f, (i0 / 1023f));

        Color c = Color.HSVToRGB(h, 1f, 1f);

        // write the output values so that 
        // 9 - Red
        // 10 - Green
        // 11 - Blue
        
        // and scale the values up so the range is 0-255
        output[9] = (int)(c.r * 255f);
        output[10] = (int)(c.g * 255f);
        output[11] = (int)(c.b * 255f);

        return output;
    }
}
