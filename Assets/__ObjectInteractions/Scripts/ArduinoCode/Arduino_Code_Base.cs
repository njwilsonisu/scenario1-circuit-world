﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Arduino_Code_Base : MonoBehaviour
{
    protected int[] output = new int[14];

    public abstract int[] ProcessInputs(int[] inputInfo);
}
