﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class Wire_Component : Wire_Base
{
    // this class extends the functionality of the basic wire
    // Used to manage the wires that are attached to a component that can be plugged into different locations

    // For this wire
    //      - Plug[0] is connected to the component
    //      - Plug[1] is the "ConnectorPlug" - the normal wire plug used to connect the component


    /// <summary>
    /// places the connector plug at the default position
    /// </summary>
    public virtual void SetDefaultPosition()
    {
        // the first plug is connected to the component, pointing down

        // Disable the second plug
        GetPlug<Plug_Wire>(1).gameObject.SetActive(false);

        // Place the second plug
        GetPlug<Plug_Wire>(1).transform.localPosition = GetPlug<Plug_Wire>(0).transform.localPosition + GetPlug<Plug_Wire>(0).transform.up * 0.05f;
        
        // make sure it is pointing up
        GetPlug<Plug_Wire>(1).transform.up = -GetPlug<Plug_Wire>(0).transform.up;

        GetPlug<Plug_Wire>(1).gameObject.SetActive(true);

        // Update the nodes
        UpdateSplineNodes();
    }

    public virtual void Activate()
    {
        SetPlugGrabbable(1, true);
    }
}
