﻿using SharpCircuit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arduino_Ground : ElectricalComponent
{
    public override CircuitElement GetCircuitElement()
    {
        Ground g = new Ground();
        return g;
    }
}
