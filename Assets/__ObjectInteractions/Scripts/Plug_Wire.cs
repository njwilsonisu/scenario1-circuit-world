﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plug_Wire : Plug_Base
{
    // The transform of where the wire should "come out" of this plug
    public Transform endPoint;

    protected virtual void Update()
    {
        // follow the connected plug
        if (isConnected)
        {
            if (transform.position != connectedPlug.transform.position)
                transform.position = connectedPlug.transform.position;
        }
    }

}
