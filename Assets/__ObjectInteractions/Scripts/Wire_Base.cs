﻿using SharpCircuit;
using SplineMesh;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRTK;

[ExecuteInEditMode]
public class Wire_Base : ElectricalComponent
{
    public bool follow = false;
    public Color wireColor = Color.grey;
    public float nodeDirectionMultiplier = 0.25f;
    public float midpointMultiplier = 0.25f;

    protected Spline spline;

    public virtual void SetWireColor(Color c)
    {
        // Get the wire renderer
        Renderer r = spline.gameObject.GetComponentInChildren<Renderer>();

        // Create a new MatPropBlock
        MaterialPropertyBlock mpb = new MaterialPropertyBlock();

        r.GetPropertyBlock(mpb);

        if (mpb.GetColor("_Color") != c)
        {
            mpb.SetColor("_Color", c);

            r.SetPropertyBlock(mpb);
        }
    }

    protected override void Awake()
    {
        base.Awake();

        if (spline == null)
        {
            spline = gameObject.GetComponentInChildren<Spline>();
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        SetWireColor(wireColor);
    }

    protected virtual void Update()
    {
        if (follow)
        {
            // Update the node positions
            UpdateSplineNodes();
        }
    }

    public virtual void UpdateSplineNodes()
    {
        if (spline == null)
        {
            spline = gameObject.GetComponentInChildren<Spline>();
        }

        // Make sure the spline contains 3 nodes
        if (spline.nodes.Count != 2)
        {
            RegenerateNodes(2);
        }

        // Get the distance
        float distance = Vector3.Distance(GetPlug<Plug_Wire>(0).endPoint.position, GetPlug<Plug_Wire>(1).endPoint.position);

        // Set the multiplier based on the distance
        float multiplier = distance * nodeDirectionMultiplier;

        // Set the first node
        SetNodeFromTransform(spline.nodes[0], GetPlug<Plug_Wire>(0).endPoint, true, multiplier);

        // Then the second
        SetNodeFromTransform(spline.nodes[1], GetPlug<Plug_Wire>(1).endPoint, false, multiplier);
    }

    protected virtual void SetNodeFromTransform(SplineNode node, Transform t, bool start, float multiplier)
    {
        // 1 for true || -1 for false
        int i = -1 + Convert.ToInt16(start) * 2;

        // get the position above/below the transform to use as the direction
        Vector3 direction = t.position + i * t.up * multiplier;

        // Set the node position
        node.Position = transform.InverseTransformPoint(t.position);
        // Set the node direction
        node.Direction = transform.InverseTransformPoint(direction);
        // Set the node up
        node.Up = Vector3.forward;
    }

    protected virtual void RegenerateNodes(int numberOfNodes)
    {
        // get the difference between the counts
        int dif = numberOfNodes - spline.nodes.Count;

        // remove nodes if necessary
        for (int i = 0; i > dif; i--)
        {
            spline.RemoveNode(spline.nodes[spline.nodes.Count - 1]);
        }

        // add nodes if necessary
        for (int i = 0; i < dif; i++)
        {
            spline.AddNode(new SplineNode(Vector3.zero, Vector3.up));
        }
    }

    public virtual bool GetGrabbablePlug(int plug, out Plug_Wire_Grabbable p)
    {
        p = GetPlug<Plug_Wire_Grabbable>(plug);

        return p != null;
    }

    /// <summary>
    /// sets the given plug isGrabbable value to the given bool value
    /// </summary>
    /// <param name="plug"></param> which plug? - 0 or 1
    /// <param name="grabbable"></param> whether the plug should be grabbable
    public virtual void SetPlugGrabbable(int plug, bool grabbable)
    {
        if (GetGrabbablePlug(plug, out Plug_Wire_Grabbable p))
        {
            // set the grabbable
            p.interactableObject.isGrabbable = grabbable;
            ListenForPlugGrabbbed(plug, OnPlugGrabbed, grabbable);
            ListenForPlugUngrabbbed(plug, OnPlugUngrabbed, grabbable);
        }
    }

    public virtual void ListenForPlugGrabbbed(int plug, PlugEventHandler listener, bool listen)
    {
        if (GetGrabbablePlug(plug, out Plug_Wire_Grabbable p))
        {
            if (listen)
            {
                p.PlugGrabbed += listener;
            }
            else
            {
                p.PlugGrabbed -= listener;
            }
        }
    }

    public virtual void ListenForPlugUngrabbbed(int plug, PlugEventHandler listener, bool listen)
    {
        if (GetGrabbablePlug(plug, out Plug_Wire_Grabbable p))
        {
            if (listen)
            {
                p.PlugUngrabbed += listener;
            }
            else
            {
                p.PlugUngrabbed -= listener;
            }
        }
    }

    protected virtual void OnPlugGrabbed(PlugEventArgs e)
    {
        follow = true;
    }

    protected virtual void OnPlugUngrabbed(PlugEventArgs e)
    {
        // do nothing
    }

    protected IEnumerator WaitToStopFollow(float duration)
    {
        yield return new WaitForSeconds(duration);
        follow = false;
    }


    protected override void OnComponentConnected()
    {
        base.OnComponentConnected();

        follow = false;

        // reset the component to be at the midpoint
        ResetWirePosition();
    }

    protected override void OnComponentDisconnected()
    {
        base.OnComponentDisconnected();

        follow = true;
    }

    public virtual void ResetWirePosition()
    {
        // get all the global positions
        List<Vector3> pos = plugs.Select(p => p.transform.position).ToList();

        // get the midpoint
        Vector3 mid = GetWireMidpoint();

        // position the component at the midpoint
        transform.position = mid;

        // reset all of the plug positions
        for (int i = 0; i < plugs.Length; i++)
        {
            plugs[i].transform.position = pos[i];
        }

        UpdateSplineNodes();
    }

    public virtual void ResetWirePosition2()
    {
        // get a list of the wire plug transforms
        List<Transform> tfs = plugs.Select(p => p.transform).ToList();

        // get all the global positions
        List<Vector3> pos = tfs.Select(tf => tf.position).ToList();

        // get the midpoint
        Vector3 mid = tfs.RaisedMidpoint(midpointMultiplier);

        // position the component at the midpoint
        transform.position = mid;

        // reset all of the plug positions
        for (int i = 0; i < tfs.Count; i++)
        {
            tfs[i].position = pos[i];
        }

        UpdateSplineNodes();
    }

    public virtual Vector3 GetWireMidpoint()
    {
        if (spline != null)
        {
            spline = gameObject.GetComponentInChildren<Spline>();
        }

        // get the middlepoint between the nodes
        Vector3 midPoint = transform.TransformPoint(spline.GetSampleAtDistance(spline.Length / 2).location);
        return midPoint;
    }

    public override CircuitElement GetCircuitElement()
    {
        Wire w = new Wire();
        return w;
    }

}
