﻿using SharpCircuit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class Component_Potentiometer : Component_Base
{
    // For this Component we have a second interactable object that
    //      will be used to calculate the potentiometers position

    // The user "presses" the potentiometer with the "Use" button

    [Header("Potentiometer Component")]

    public VRTK_InteractableObject inputArea;

    public GameObject touchPointFollower;

    // is the input area is being touched
    protected bool isTouched = false;

    // is the softpot being pressed
    protected bool isPressed = false;

    // the location of the presser
    protected Transform presser;

    protected float[] range = new float[2];

    protected Collider iACollider;

    // the calculated potentiometer values between 0 and 1
    public float potPosition = 1.0f;
    public float previousPotPosition = 0.0f;

    // a reference to the potentiometer circuit element
    protected Potentiometer element;

    public override CircuitElement GetCircuitElement()
    {
        element = new Potentiometer();
        return element;
    }

    protected override void OnEnable()
    {
        if (iACollider == null)
        {
            iACollider = inputArea.gameObject.GetComponent<Collider>();
        }

        SetRange();

        // Listen for the object being touched/used
        inputArea.InteractableObjectTouched += OnTouched;
        inputArea.InteractableObjectUntouched += OnUntouched;
        inputArea.InteractableObjectUsed += OnPressed;
        inputArea.InteractableObjectUnused += OnUnpressed;

        // setup the press location follower
        inputArea.gameObject.GetComponent<VRTK_InteractObjectHighlighter>().objectToHighlight = touchPointFollower;

        touchPointFollower.SetActive(false);

        base.OnEnable();
    }

    protected virtual void SetRange()
    {
        // get the bounds for the collider
        Bounds b = iACollider.bounds;

        range[0] = inputArea.transform.InverseTransformPoint(b.min).y;
        range[1] = inputArea.transform.InverseTransformPoint(b.max).y;

    }

    protected override void OnDisable()
    {
        inputArea.InteractableObjectUsed -= OnPressed;
        inputArea.InteractableObjectUnused -= OnUnpressed;

        base.OnDisable();
    }

    protected override void OnComponentConnected()
    {
        base.OnComponentConnected();

        iACollider.enabled = true;
    }

    protected override void OnComponentDisconnected()
    {
        base.OnComponentDisconnected();

        iACollider.enabled = false;
    }

    public virtual void OnTouched(object sender, InteractableObjectEventArgs e)
    {
        // get the presser
        presser = e.interactingObject.GetComponent<VRTK_BasePointerRenderer>().GetObjectInteractor().transform;

        // set bool
        isTouched = true;

        touchPointFollower.SetActive(true);
    }


    public virtual void OnUntouched(object sender, InteractableObjectEventArgs e)
    {
        presser = null;

        isTouched = false;

        touchPointFollower.SetActive(false);
    }

    public virtual void OnPressed(object sender, InteractableObjectEventArgs e)
    {
        if(presser == null)
        {
            // get the presser
            presser = e.interactingObject.GetComponent<VRTK_BasePointerRenderer>().GetObjectInteractor().transform;
        }

        // set bool
        isPressed = true;
    }


    public virtual void OnUnpressed(object sender, InteractableObjectEventArgs e)
    {
        isPressed = false;
    }


    protected virtual void Update()
    {
        if(element == null)
        {
            GetCircuitElement();
        }

        if (isTouched)
        {
            // get the relative position of where the user is pressing the potentiometer
            Vector3 p = inputArea.transform.InverseTransformPoint(presser.position);

            // place the follow there
            touchPointFollower.transform.localPosition = p;
        }

        // if the user is currently pressing the input area...
        if (isPressed)
        {
            // get the value from the local position of the follower
            potPosition = (touchPointFollower.transform.localPosition.y - range[0]) / (range[1] - range[0]);
        }
        // if they arent...
        else
        {
            // reset the potPosition to 0
            potPosition = 1.0f;
        }

        // keep it withing range
        potPosition = Mathf.Clamp(potPosition, 0f, 1f);

        if (potPosition != previousPotPosition)
        {
            // set the new position
            element.position = (double)potPosition;

            // flag
            needsUpdating = true;

            previousPotPosition = potPosition;
        }
        //else
        //{
        //    needsUpdating = false;
        //}
    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
        
        Gizmos.color = Color.red;

        var b = inputArea.transform.TransformPoint(new Vector3(0, range[0], 0));
        var t = inputArea.transform.TransformPoint(new Vector3(0, range[1], 0));
        Gizmos.DrawSphere(b, 0.0025f);
        Gizmos.DrawSphere(t, 0.0025f);
        Gizmos.DrawLine(b, t);
    }
}
