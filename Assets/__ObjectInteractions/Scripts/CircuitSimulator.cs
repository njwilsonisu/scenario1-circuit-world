﻿using SharpCircuit;
using System;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using static SharpCircuit.Circuit;

[Serializable]
public class ForcedConnection
{
    // when forced, plug 1 -> plug 2
    public Plug_Base plug1;
    public Plug_Base plug2;
}

public class CircuitSimulator : MonoBehaviour
{
    // The interactable object for grabbing and placing this circuit
    protected VRTK_InteractableObject interactableObject;

    // Where new components can be added to the circuit
    public SnapDropZone componentDropZone;

    // Where prebuilt circuit components are held
    public GameObject componentsHolder;

    

    // the circuit
    public Circuit circuit;

    // List of all the components contained and processed
    public List<ElectricalComponent> allComponents = new List<ElectricalComponent>();

    // List of all the components that are actually in the ciruit simulator
    public List<ElectricalComponent> circuitComponents = new List<ElectricalComponent>();

    // List of all the connections within the circuit
    public List<CircuitConnection> circuitConnections = new List<CircuitConnection>();

    // whether or not the circuit is running
    protected bool isRunning = false;

    // whether or not the circuit needs to analyze
    protected bool analyzeFlag = false;


    [Header("FOR TESTING")]
    // -------------- FOR TESTING ---------------------
    public bool initializeCircuitOnStart = true;
    // -------------- ----------- ---------------------


    // at start
    protected virtual void Start()
    {
        if(interactableObject == null)
        {
            interactableObject = GetComponent<VRTK_InteractableObject>();
        }

        componentDropZone.gameObject.SetActive(false);

        if (initializeCircuitOnStart)
        {
            InitializeCircuit();
        }
    }

    protected virtual void OnEnable()
    {
        componentDropZone.ListenForObjectSnapped(OnComponentSnapped, true);
    }

    protected virtual void OnDisable()
    {
        componentDropZone.ListenForObjectSnapped(OnComponentSnapped, false);
    }

    public virtual void InitializeCircuit()
    {
        // disable the interactable object
        interactableObject.disableWhenIdle = false;
        interactableObject.enabled = false;

        // enable the drop zone
        componentDropZone.gameObject.SetActive(true);

        // clear the list of components
        allComponents.Clear();

        // create a new circuit
        circuit = new Circuit();

        // for every component in the children of this component
        foreach (ElectricalComponent component in transform.GetComponentsInChildren<ElectricalComponent>())
        {
            // process the component
            ProcessComponent(component);
        }

        isRunning = true;
    }

    protected virtual void Update()
    {
        if (circuit != null)
        {
            analyzeFlag = analyzeFlag || ComponentsNeedUpdating();

            if (analyzeFlag)
            {
                bool success = true;
                //Debug.LogError("Analyzing...");
                try
                {
                    circuit.analyze();
                }
                catch (Circuit.Exception e)
                {
                    Debug.LogError(e.Message);
                    success = false;
                }

                // only run if the analysis was successful
                isRunning = success;

                analyzeFlag = false;
            }


            if (isRunning)
            {
                //Debug.LogError("Tick");
                try
                {
                    circuit.doTick();
                }
                catch
                {
                    // DO SOMETHING
                }
            }
        }
    }

    public virtual void ProcessComponent(ElectricalComponent component)
    {
        // if the component is not already accounted for...
        if (!allComponents.Contains(component))
        {
            // Add the component to the list
            allComponents.Add(component);

            // if the component is connected
            if (component.isConnected)
            {
                // add the component to the circuit
                AddCircuitElement(component);

                // Listen for if the component is disconnected
                component.ListenForComponentDisconnected(OnComponentDisconnected, true);

                // go through the components plugs
                foreach (Plug_Base plug in component.plugs)
                {
                    if (plug != null && plug.isConnected)
                    {
                        // process the plugs lead
                        ProcessLead(plug.lead);
                    }
                }
            }
            // if the component is NOT connected
            else
            {
                component.ListenForComponentConnected(OnComponentConnected, true);
            }
        }
    }

    public virtual void ProcessLead(ElectricalLead lead)
    {
        //Debug.LogError("Process Lead");

        // if the given lead is connected...
        if (lead.IsConnected())
        {
            //Debug.LogError("Lead is connected");
            // for each lead the given lead is connected to...
            foreach (ElectricalLead connectedLead in lead.connectedLeads)
            {
                ProcessConnection(lead, connectedLead);
            }
        }
    }

    public virtual void ProcessConnection(ElectricalLead lead1, ElectricalLead lead2)
    {
        // make sure the connected component has been processed
        ElectricalComponent c = lead2.owner;
        if (!allComponents.Contains(c))
        {
            ProcessComponent(c);
        }

        //Debug.LogError("Process Connection");

        // create a new connection
        CircuitConnection newConnection = new CircuitConnection(lead1, lead2);

        // if the connection has not been processed yet...
        if (!circuitConnections.Contains(newConnection))
        {
            // add the connection to the circuit
            AddCircuitConnection(newConnection);

            // add the connection to the list
            circuitConnections.Add(newConnection);
        }
    }

    protected virtual void AddCircuitElement(ElectricalComponent component)
    {
        if (circuit != null)
        {
            //Debug.LogError("Adding a new " + component.circuitElement.GetType().Name + " to the circuit");

            // add the element to the circuit
            circuit.AddElement(component.circuitElement);

            if (!circuitComponents.Contains(component))
            {
                // add the element to the list
                circuitComponents.Add(component);
            }

            analyzeFlag = true;
        }
    }

    protected virtual void AddCircuitConnection(CircuitConnection connection)
    {
        //Debug.LogError("Adding a new connection to the circuit: " + connection.component1.circuitElement + "." + connection.lead1.index + " to " + connection.component2.circuitElement + "." + connection.lead2.index);

        // add the connection
        circuit.Connect(connection.component1.circuitElement, connection.lead1.index, connection.component2.circuitElement, connection.lead2.index);

        analyzeFlag = true;
    }

    

    protected virtual void OnComponentSnapped(object sender, SnapDropZoneEventArgs e)
    {
        // parent the component
        //e.snappedObject.transform.parent = transform;

        // get the component
        var component = e.snappedObject.GetComponent<ElectricalComponent>();
        if (component != null)
        {
            // process the new component
            ProcessComponent(component);
        }
    }

    public virtual void OnComponentConnected(ElectricalComponentEventArgs e)
    {
        ElectricalComponent component = e.sender;
        ProcessComponent(component);
    }

    public virtual void OnComponentDisconnected(ElectricalComponentEventArgs e)
    {
        // get the disconnected component
        ElectricalComponent disconnectedComponent = e.sender;

        if (circuitComponents.Contains(disconnectedComponent))
        {
            // remove the component from the circuit components list
            circuitComponents.Remove(disconnectedComponent);

            // remove all the connections that included that component
            circuitConnections.RemoveAll(leadConnection => leadConnection.component1 == disconnectedComponent || leadConnection.component2 == disconnectedComponent);
        }

        //if(circuit != null && isRunning)

        // stop running the circuit
        isRunning = false;

        // create a new circuit
        circuit = new Circuit();

        // Add all of the circuit elements
        foreach(ElectricalComponent component in circuitComponents)
        {
            AddCircuitElement(component);
        }

        // Add all of the connections
        foreach(CircuitConnection connection in circuitConnections)
        {
            AddCircuitConnection(connection);
        }

        // restart the circuit
        isRunning = true;

    }

    public virtual void ForceNewConnections(List<ForcedConnection> newConnections)
    {
        // make sure we have a component holder
        if (componentsHolder == null)
        {
            componentsHolder = new GameObject();

            componentsHolder.transform.parent = transform;

            componentsHolder.name = "CircuitComponents";
        }

        List<bool> successes = new List<bool>();

        for (int i = 0; i < newConnections.Count; i++)
        {
            successes.Add(false);

            var c = newConnections[i];
            if (c != null && c.plug1 != null && c.plug2 != null)
            {
                // disconnect the plugs
                c.plug1.DisconnectFromPlug();
                c.plug2.DisconnectFromPlug();

                // forcefully connect the first plug to the second
                c.plug1.ConnectToPlug(c.plug2, true);

                successes[i] = true;
            }
        }

    }

    protected virtual bool ComponentsNeedUpdating()
    {
        // if any of the components need updating...
        foreach (ElectricalComponent component in circuitComponents)
        {
            if (component.NeedsUpdating())
            {
                //Debug.LogError("Needs Updating: " + component.gameObject.name);
                return true;
            }
        }

        // otherwise
        return false;
    }

    protected virtual void OnDrawGizmos()
    {
        if (circuitConnections != null && allComponents != null)
        {
            Gizmos.color = Color.green;
            for (int i = 0; i < circuitConnections.Count; i++)
            {
                Gizmos.DrawLine(circuitConnections[i].lead1.plug.transform.position, circuitConnections[i].lead2.plug.transform.position);
            }

            for (int i = 0; i < allComponents.Count; i++)
            {
                var cp = allComponents[i].transform.position;
                var c = true;
                foreach (Plug_Base plug in allComponents[i].plugs)
                {
                    if (plug != null)
                    {
                        if (plug.isConnected)
                        {
                            Gizmos.color = Color.green;
                            // get the lead voltage
                            var v = plug.lead.owner.circuitElement.getLeadVoltage(plug.lead.index);
                            if (v > 0)
                            {
                                Gizmos.color = Color.yellow;
                            }
                        }
                        else
                        {
                            Gizmos.color = Color.red;
                            c = false;
                        }

                        var pp = plug.transform.position;
                        Gizmos.DrawSphere(pp, 0.0005f);
                        Gizmos.DrawLine(cp, pp);
                    }
                }

                if (c)
                {
                    Gizmos.color = Color.green;
                }
                else
                {
                    Gizmos.color = Color.red;
                }
                Gizmos.DrawSphere(cp, 0.002f);
            }
        }

    }

}
