﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class TemporaryPlayerManager : MonoBehaviour
{
    public Transform headset;
    public Transform centerOfPlayAreaTransform;
    public Transform rightController;
    public Transform leftController;

    public SDK_BaseHeadset.HeadsetType headsetType;

    private void OnEnable()
    {
        // when a new SDK is loaded (when the user chooses to go into VR mode)
        VRTK_SDKManager.SubscribeLoadedSetupChanged(OnLoadedSetupChanged);
    }

    private void OnDisable()
    {
        VRTK_SDKManager.UnsubscribeLoadedSetupChanged(OnLoadedSetupChanged);
    }


    protected virtual void OnLoadedSetupChanged(VRTK_SDKManager sender, VRTK_SDKManager.LoadedSetupChangeEventArgs e)
    {
        GetDeviceTransforms();
    }


    protected virtual void GetDeviceTransforms()
    {
        // Most of these come from here
        // https://vrtoolkit.readme.io/docs/vrtk_devicefinder

        try
        {
            // gets the current headset transform
            headset = VRTK_DeviceFinder.DeviceTransform(VRTK_DeviceFinder.Devices.Headset);

            // gets the sdk type
            headsetType = VRTK_DeviceFinder.GetHeadsetType();

            // This is the primary controller for the simulator
            rightController = VRTK_DeviceFinder.DeviceTransform(VRTK_DeviceFinder.Devices.RightController);

            // Left controller is only used when in VR
            leftController = VRTK_DeviceFinder.DeviceTransform(VRTK_DeviceFinder.Devices.LeftController);
        }
        catch{}

    }

    // When the player selects "VR Mode" use this to load the new SDK
    protected virtual void LoadNewSetup()
    {
        
        //VRTK_SDKSetup[] setups = VRTK_SDKManager.GetAllSDKSetups();

        // if we know the index
        //VRTK_SDKManager.AttemptTryLoadSDKSetup(indexCopy, true, setups)

        // load any different SDK if it is available
        //VRTK_SDKManager.AttemptTryLoadSDKSetupFromList(false);
    }
}
