﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using VRTK;

public static class TransformHelpers
{
    public static void ResetLocal(this Transform transform, bool pos = true, bool rot = true, bool scl = true)
    {
        if (pos)
            transform.localPosition = Vector3.zero;
        if (rot)
            transform.localRotation = Quaternion.identity;
        if (scl)
            transform.localScale = Vector3.one;
    }

    public static void ResetGlobal(this Transform transform, bool pos = true, bool rot = true, bool scl = true)
    {
        if (pos)
            transform.position = Vector3.zero;
        if (rot)
            transform.rotation = Quaternion.identity;
        if (scl)
            transform.SetGlobalScale(Vector3.one);
    }

    /// <summary>
    /// Places the transform in the same hierachy position, and copies the local transform info
    /// </summary>
    /// <param name="copyTo"></param>
    /// <param name="copyMe"></param>
    /// <param name="createNew"></param>
    public static void CopyLocalTransform(this Transform copyTo, Transform copyMe, bool pos = true, bool rot = true, bool scl = true)
    {
        copyTo.parent = copyMe.parent;

        if (pos)
            copyTo.localPosition = copyMe.localPosition;
        if (rot)
            copyTo.localRotation = copyMe.localRotation;
        if (scl)
            copyTo.localScale = copyMe.localScale;
    }

    public static void CopyGlobalTransform(this Transform copyTo, Transform copyMe, bool pos = true, bool rot = true, bool scl = true)
    {
        if (pos)
            copyTo.position = copyMe.position;
        if (rot)
            copyTo.rotation = copyMe.rotation;
        if (scl)
            copyTo.SetGlobalScale(copyMe.lossyScale);
    }

    public static Transform CreateEmptyTransformCopy(this Transform copyMe)
    {
        GameObject temp = new GameObject(copyMe.name + "_COPY");
        Transform copy = temp.transform;

        copy.CopyLocalTransform(copyMe);

        return copy;
    }

    public static float DistanceToHeadset(this Transform transform)
    {
        // get headset
        Transform headset = VRTK_DeviceFinder.DeviceTransform(VRTK_DeviceFinder.Devices.Headset);
        return Vector3.Distance(transform.position, headset.position);
    }

    public static float DistanceToHeadset(this Vector3 position)
    {
        // get headset
        Transform headset = VRTK_DeviceFinder.DeviceTransform(VRTK_DeviceFinder.Devices.Headset);
        return Vector3.Distance(position, headset.position);
    }

    public static Vector3 TrueClosestPoint(this Collider c, Vector3 position)
    {
        RaycastHit hit;
        Physics.Raycast(c.transform.position, Vector3.zero - (position - c.transform.position).normalized, out hit);
        return hit.point;
    }

    public static Transform[] MinimumDistance(this List<Transform> tfs)
    {
        Transform[] min = new Transform[2];

        float minDistance = float.PositiveInfinity;

        for (int i = 0; i < tfs.Count; i++)
        {
            for (int j = 1; j < tfs.Count - i; j++)
            {
                float d = Vector3.Distance(tfs[i].position, tfs[i + j].position);

                if (d < minDistance)
                {
                    minDistance = d;
                    min[0] = tfs[i];
                    min[1] = tfs[i + j];
                }
            }
        }

        return min;
    }

    public static Transform[] MaximumDistance(this List<Transform> tfs)
    {
        Transform[] max = new Transform[2];

        float maxDistance = 0f;

        for (int i = 0; i < tfs.Count; i++)
        {
            for (int j = 1; j < tfs.Count - i; j++)
            {
                float d = Vector3.Distance(tfs[i].position, tfs[i + j].position);

                if (d > maxDistance)
                {
                    maxDistance = d;
                    max[0] = tfs[i];
                    max[1] = tfs[i + j];
                }
            }
        }

        return max;
    }

    public static Vector3 RaisedMidpoint(this List<Transform> tfs, float multiplier)
    {
        // vectors for storing minimum and maximum position values
        Vector3 minPoint = tfs[0].position;
        Vector3 maxPoint = tfs[0].position;

        // vector for storing up directions
        Vector3 u = Vector3.Normalize(tfs[0].up);

        for (int i = 1; i < tfs.Count; i++)
        {
            Vector3 pos = tfs[i].position;
            if (pos.x < minPoint.x)
                minPoint.x = pos.x;
            if (pos.x > maxPoint.x)
                maxPoint.x = pos.x;
            if (pos.y < minPoint.y)
                minPoint.y = pos.y;
            if (pos.y > maxPoint.y)
                maxPoint.y = pos.y;
            if (pos.z < minPoint.z)
                minPoint.z = pos.z;
            if (pos.z > maxPoint.z)
                maxPoint.z = pos.z;

            u += Vector3.Normalize(tfs[i].up);
        }

        // normalize the up vector
        u = Vector3.Normalize(u);

        // set the midpoint at the center of x and z but at the highest y
        Vector3 midPosition = new Vector3(Mathf.Lerp(minPoint.x, maxPoint.x, 0.5f), maxPoint.y, Mathf.Lerp(minPoint.z, maxPoint.z, 0.5f));

        float maxDistance = 0f;

        // get the 2 actual max points
        Transform[] max = tfs.MaximumDistance();

        if(max[0] != null && max[1] != null)
        {
            // get the max distance
            maxDistance = Vector3.Distance(max[0].position, max[1].position);
        }

        // clamp it
        maxDistance = Mathf.Clamp(maxDistance, 0.05f, 100f);

        // set the height based on the distance
        midPosition += u * multiplier * Mathf.Sqrt(maxDistance);

        return midPosition;
    }

    public static IEnumerator Tween(GameObject obj, Vector3 endPosition, Quaternion endRotation, float duration)
    {
        float elapsedTime = 0f;

        Transform ioTransform = obj.transform;

        Vector3 startPosition = ioTransform.position;
        Quaternion startRotation = ioTransform.rotation;
        //Vector3 startScale = ioTransform.localScale;

        while (elapsedTime <= duration)
        {
            elapsedTime += Time.deltaTime;

            ioTransform.position = Vector3.Lerp(startPosition, endPosition, (elapsedTime / duration));
            ioTransform.rotation = Quaternion.Lerp(startRotation, endRotation, (elapsedTime / duration));
            //ioTransform.localScale = Vector3.Lerp(startScale, endScale, (elapsedTime / duration));

            yield return null;
        }

        //Force all to the last setting in case anything has moved during the transition
        ioTransform.position = endPosition;
        ioTransform.rotation = endRotation;
        //ioTransform.localScale = endScale;
    }

    public static IEnumerator TweenLocal(GameObject obj, Vector3 endPosition, Quaternion endRotation, float duration)
    {
        float elapsedTime = 0f;

        Transform ioTransform = obj.transform;

        Vector3 startPosition = ioTransform.localPosition;
        Quaternion startRotation = ioTransform.localRotation;
        //Vector3 startScale = ioTransform.localScale;

        while (elapsedTime <= duration)
        {
            elapsedTime += Time.deltaTime;

            ioTransform.localPosition = Vector3.Lerp(startPosition, endPosition, (elapsedTime / duration));
            ioTransform.localRotation = Quaternion.Lerp(startRotation, endRotation, (elapsedTime / duration));
            //ioTransform.localScale = Vector3.Lerp(startScale, endScale, (elapsedTime / duration));

            yield return null;
        }

        //Force all to the last setting in case anything has moved during the transition
        ioTransform.localPosition = endPosition;
        ioTransform.localRotation = endRotation;
        //ioTransform.localScale = endScale;
    }

}
