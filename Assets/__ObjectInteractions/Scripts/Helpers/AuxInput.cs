﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Auxillary Input manager to enable simulated key presses
/// </summary>
public class AuxInput : MonoBehaviour
{

    private static string keyPressed = "";
    private static string keyDown = "";
    private static string keyUp = "";

    public static void SetKey(KeyCode key)
    {
        keyPressed = keyPressed + key.ToString() + Time.frameCount + " ";
        
        //Debug.LogError(keyPressed);

        if (keyPressed.Length > 100)
        {
            keyPressed = keyPressed.Remove(0, keyPressed.Length - 100);
        }
    }

    public static void SetKeyDown(KeyCode key)
    {
        keyDown = keyDown + key.ToString() + Time.frameCount + " ";

        //Debug.LogError(keyDown);

        if (keyDown.Length > 100)
        {
            keyDown = keyDown.Remove(0, keyDown.Length - 100);
        }
    }

    public static void SetKeyUp(KeyCode key)
    {
        keyUp = keyUp + key.ToString() + Time.frameCount + " ";

        //Debug.LogError(keyUp);

        if (keyUp.Length > 100)
        {
            keyUp = keyUp.Remove(0, keyUp.Length - 100);
        }
    }

    public static bool GetKey(KeyCode key)
    {
        bool pressed = false;

        if (Input.GetKey(key))
        {
            ////Debug.LogError("Pressed: " + key);
            pressed = true;
        }
        else if(keyPressed.Contains(key.ToString() + Time.frameCount))
        {
            ////Debug.LogError("Forced Pressed: " + key);
            pressed = true;
        }

        return pressed;
    }

    public static bool GetKeyUp(KeyCode key)
    {
        bool pressed = false;

        if (Input.GetKeyUp(key))
        {
            //Debug.LogError("Up: " + key);
            pressed = true;
        }
        else if (keyUp.Contains(key.ToString() + Time.frameCount))
        {
            //Debug.LogError("Forced Up: " + key);
            pressed = true;
        }

        return pressed;
    }

    public static bool GetKeyDown(KeyCode key)
    {
        bool pressed = false;

        if (Input.GetKeyDown(key))
        {
            //Debug.LogError("Down: " + key);
            pressed = true;
        }
        else if (keyDown.Contains(key.ToString() + Time.frameCount))
        {
            //Debug.LogError("Forced Down: " + key);
            pressed = true;
        }

        return pressed;
    }

}
