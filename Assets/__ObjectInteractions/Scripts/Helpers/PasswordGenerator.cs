﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class PasswordGenerator
{
    private static string alphaNumeric = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static string GeneratePassword(int length)
    {
        string pw = "";
        for(int i = 0; i < length; i++)
        {
            int characterIndex = Random.Range(0, alphaNumeric.Length);
            pw += alphaNumeric[characterIndex];
        }

        return pw;
        
    }
}
