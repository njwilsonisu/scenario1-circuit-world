﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void KeystrokerCallback();

public class Keystroker
{
    private int currentKey = 0;

    private MonoBehaviour controller;

    private List<KeyCode> keys;
    private float wait;
    private KeystrokerCallback Callback;

    private IEnumerator enumerator;

    public Keystroker(MonoBehaviour controller, List<KeyCode> keys, float wait, KeystrokerCallback callback)
    {
        this.controller = controller;
        this.keys = keys;
        this.wait = wait;
        this.Callback = callback;

        // first key already pressed -> wait for second
        currentKey = 0;
        KeyPressed();

    }

    public IEnumerator WaitForNextKeystroke(KeyCode key)
    {
        float elapsedTime = 0f;

        while(elapsedTime < wait)
        {
            if (Input.GetKeyDown(key))
            {
                KeyPressed();
                yield break;
            }
            else if (Input.anyKeyDown && !Input.inputString.Equals("") && !Input.inputString.Equals(keys[currentKey-1].ToString().ToLower()))
            {
                //Debug.LogError("Other Key Pressed: " + Input.inputString);
                break;
            }

            elapsedTime += Time.deltaTime;

            yield return null;
        }

        KeyNotPressed();
    }


    private void KeyPressed()
    {
        //Debug.LogError("Key Pressed: " + keys[currentKey].ToString());

        int nextKey = currentKey + 1;
        
        if(nextKey < keys.Count)
        {
            currentKey = nextKey;
            enumerator = WaitForNextKeystroke(keys[nextKey]);
            controller.StartCoroutine(enumerator);
        }
        else
        {
            Callback();
        }
    }

    private void KeyNotPressed()
    {
        //Debug.LogError("Key Not Pressed");
        currentKey = 0;
    }
}
