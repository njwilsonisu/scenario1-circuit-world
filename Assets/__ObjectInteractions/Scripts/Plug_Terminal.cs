﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class Plug_Terminal : Plug_Base
{
    [Header("Plug Drop Zone Prefab")]
    public GameObject PlugDropZonePrefab;

    // the interactable object connected to this gameobject
    protected SnapDropZone snapDropZone;

    public event PlugEventHandler PlugEnteredZone;
    public event PlugEventHandler PlugExitedZone;

    public override bool IsStationary()
    {
        return true;
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        // instantiate the drop zone as a child of this object
        GameObject g = Instantiate(PlugDropZonePrefab, transform);

        // get the drop zone script
        snapDropZone = g.GetComponent<SnapDropZone>();

        // if this plug is connected at start
        if (isConnected && connectedPlug != null)
        {
            // force the connected plug to snap into the drop zone
            snapDropZone.ForceSnap(connectedPlug.gameObject);
        }

        // subscribe to the events
        ListenForPlugEntered(true);
        ListenForPlugExited(true);
        ListenForPlugSnapped(true);
        ListenForPlugUnsnapped(true);
    }

    protected override void OnDisable()
    {
        if (snapDropZone != null)
        {
            // unsubscribe to the events
            ListenForPlugEntered(false);
            ListenForPlugExited(false);
            ListenForPlugSnapped(false);
            ListenForPlugUnsnapped(false);

            DestroyImmediate(snapDropZone.gameObject);
        }

        base.OnDisable();
    }

    protected virtual void ListenForPlugEntered(bool listen)
    {
        if (listen)
        {
            snapDropZone.ObjectEnteredSnapDropZone += OnPlugEnteredZone;
        }
        else
        {
            snapDropZone.ObjectEnteredSnapDropZone -= OnPlugEnteredZone;
        }
    }

    protected virtual void ListenForPlugExited(bool listen)
    {
        if (listen)
        {
            snapDropZone.ObjectExitedSnapDropZone += OnPlugExitedZone;
        }
        else
        {
            snapDropZone.ObjectExitedSnapDropZone -= OnPlugExitedZone;
        }
    }

    protected virtual void ListenForPlugSnapped(bool listen)
    {
        if (listen)
        {
            snapDropZone.ObjectSnappedToDropZone += OnPlugSnapped;
        }
        else
        {
            snapDropZone.ObjectSnappedToDropZone -= OnPlugSnapped;
        }
    }

    protected virtual void ListenForPlugUnsnapped(bool listen)
    {
        if (listen)
        {
            snapDropZone.ObjectUnsnappedFromDropZone += OnPlugUnsnapped;
        }
        else
        {
            snapDropZone.ObjectUnsnappedFromDropZone -= OnPlugUnsnapped;
        }
    }

    protected virtual void OnPlugEnteredZone(object sender, SnapDropZoneEventArgs e)
    {
        if (PlugEnteredZone != null)
        {
            PlugEnteredZone(new PlugEventArgs { sender = this });
        }
    }

    protected virtual void OnPlugExitedZone(object sender, SnapDropZoneEventArgs e)
    {
        if (PlugExitedZone != null)
        {
            PlugExitedZone(new PlugEventArgs { sender = this });
        }
    }

    protected virtual void OnPlugSnapped(object sender, SnapDropZoneEventArgs e)
    {
        // connect to the plug
        ConnectToPlug(e.snappedObject);
    }

    protected virtual void OnPlugUnsnapped(object sender, SnapDropZoneEventArgs e)
    {
        // Disconnect from plug
        DisconnectFromPlug();
    }

    protected virtual void OnDrawGizmos()
    {
        if (!Application.isPlaying)
        {
            Gizmos.color = new Color(1,1,1,0.5f);
            Gizmos.DrawCube(transform.TransformPoint(Vector3.up * 0.01f), new Vector3(0.005f, 0.005f, 0.005f));

        }
    }
}
