﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Plug_Base), true)]
public class PlugEditor : Editor
{
    public bool connectInEditor;
    
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Plug_Base plug = (Plug_Base)target;
        if (GUILayout.Button("Force Connection"))
        {
            plug.ConnectToPlug(plug.connectedPlug.gameObject, true);
        }

        if (GUILayout.Button("Disconnect"))
        {
            plug.DisconnectFromPlug();
        }
    }
}
