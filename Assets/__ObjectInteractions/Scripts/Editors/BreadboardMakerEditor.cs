﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BreadboardMaker))]
public class BreadboardMakerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        BreadboardMaker breadboard = (BreadboardMaker)target;
        if (GUILayout.Button("Regenerate Breadboard"))
        {
            breadboard.RegenerateStrips();
        }
        
        if (GUILayout.Button("Remove All Strips"))
        {
            breadboard.RemoveAllStrips();
        }

        if (GUILayout.Button("Remove All Terminals"))
        {
            breadboard.RemoveAllTerminals();
        }
    }
}
