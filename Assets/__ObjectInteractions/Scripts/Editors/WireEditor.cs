﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Wire_Base), true)]
public class WireEditor : Editor
{
    public bool connectInEditor;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Wire_Base wire = (Wire_Base)target;
        if (GUILayout.Button("Set Color"))
        {
            wire.SetWireColor(wire.wireColor);
        }
    }
}
