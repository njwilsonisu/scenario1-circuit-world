﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CircuitSimulator), true)]
public class CircuitSimulatorEditor : Editor
{
    //string[] choices = new[] { "foo", "foobar" };

    //int choiceIndex = 0;
    //int previousChoiceIndex = 0;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CircuitSimulator t = (CircuitSimulator)target;

        if (GUILayout.Button("New Connections"))
        {
            CircuitBuilderEditorWindow menu = (CircuitBuilderEditorWindow)EditorWindow.GetWindow(typeof(CircuitBuilderEditorWindow));
            menu.SetTarget(t);
        }

        //choiceIndex = EditorGUILayout.Popup(choiceIndex, choices);

        //if(choiceIndex != previousChoiceIndex)
        //{
        //    // Update the selected choice in the underlying object
        //    t.choice = choices[choiceIndex];

        //    // Save the changes back to the object
        //    EditorUtility.SetDirty(target);

        //    previousChoiceIndex = choiceIndex;
        //}

    }
}

public class CircuitBuilderEditorWindow : EditorWindow
{
    public CircuitSimulator target;
    public List<ForcedConnection> newConnections = new List<ForcedConnection>();

    public void SetTarget(CircuitSimulator t)
    {
        target = t;
    }

    void OnGUI()
    {
        ScriptableObject scriptableObj = this;
        SerializedObject serialObj = new SerializedObject(scriptableObj);
        SerializedProperty serialProp = serialObj.FindProperty("newConnections");

        EditorGUILayout.PropertyField(serialProp, true);
        serialObj.ApplyModifiedProperties();

        if (GUILayout.Button("Create Connections"))
        {
            target.ForceNewConnections(newConnections);
        }
    }
}
