﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Component_Base), true)]
public class ComponentEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Component_Base comp = (Component_Base)target;
        
        if (GUILayout.Button("Regenerate Wires"))
        {
            comp.RegenerateWires();
        }

        if (GUILayout.Button("Reset Position"))
        {
            comp.ResetComponentPosition();
        }
    }
}
