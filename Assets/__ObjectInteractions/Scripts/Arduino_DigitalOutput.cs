﻿using SharpCircuit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arduino_DigitalOutput : ElectricalComponent
{
    protected VoltageInput element;

    [HideInInspector]
    public float outputValue = 0.0f;
    protected float previousOutputValue = 0.0f;

    // COMPUTER OUTPUT -> CIRCUIT INPUT
    public override CircuitElement GetCircuitElement()
    {
        element = new VoltageInput(Voltage.WaveType.DC);
        return element;
    }

    public virtual void UpdateOutputVoltage(float v)
    {
        // set the voltage of the element
        element.maxVoltage = (double)v;

        outputValue = v;

        // if the value is changing...
        if (outputValue != previousOutputValue)
        {
            // flag
            needsUpdating = true;

            // update previous value
            previousOutputValue = outputValue;
        }
        //else
        //{
        //    needsUpdating = false;
        //}
    }
}
