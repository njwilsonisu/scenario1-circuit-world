﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class Plug_Wire_Grabbable : Plug_Wire
{
    // the interactable object connected to this gameobject
    public VRTK_InteractableObject interactableObject;

    protected Vector3 defaultLocalPosition;
    protected Quaternion defaultLocalRotation;

    protected bool faceDown = false;

    public event PlugEventHandler PlugGrabbed;
    public event PlugEventHandler PlugUngrabbed;

    public virtual void SaveDefaultPosition()
    {
        defaultLocalPosition = transform.localPosition;
        defaultLocalRotation = transform.localRotation;
    }

    public virtual void ReturnToDefault(bool immediate = false)
    {
        if (immediate)
        {
            transform.localPosition = defaultLocalPosition;
            transform.localRotation = defaultLocalRotation;
        }
        else
        {
            StartCoroutine(TweenLocal(gameObject, defaultLocalPosition, defaultLocalRotation, 0.25f));
        }

    }

    protected override void OnEnable()
    {
        base.OnEnable();

        SaveDefaultPosition();

        // subscribe to the events
        ListenForPlugGrabbed(true);
        ListenForPlugUgrabbed(true);
        ListenForPlugSnapped(true);
        ListenForPlugUnsnapped(true);
    }

    protected override void OnDisable()
    {
        ReturnToDefault(true);

        // subscribe to the events
        ListenForPlugGrabbed(false);
        ListenForPlugUgrabbed(false);
        ListenForPlugSnapped(false);
        ListenForPlugUnsnapped(false);

        base.OnDisable();
    }

    protected override void Update()
    {
        if (faceDown)
        {
            transform.rotation = Quaternion.FromToRotation(Vector3.forward, Vector3.up) * Quaternion.LookRotation(Vector3.down);
        }

        base.Update();
    }

    protected virtual void ListenForPlugGrabbed(bool listen)
    {
        if (listen)
        {
            interactableObject.InteractableObjectGrabbed += OnPlugGrabbed;
        }
        else
        {
            interactableObject.InteractableObjectGrabbed -= OnPlugGrabbed;
        }
    }

    protected virtual void ListenForPlugUgrabbed(bool listen)
    {
        if (listen)
        {
            interactableObject.InteractableObjectUngrabbed += OnPlugUngrabbed;
        }
        else
        {
            interactableObject.InteractableObjectUngrabbed -= OnPlugUngrabbed;
        }
    }

    protected virtual void ListenForPlugSnapped(bool listen)
    {
        if (listen)
        {
            interactableObject.InteractableObjectSnappedToDropZone += OnPlugSnapped;
        }
        else
        {
            interactableObject.InteractableObjectSnappedToDropZone -= OnPlugSnapped;
        }
    }

    protected virtual void ListenForPlugUnsnapped(bool listen)
    {
        if (listen)
        {
            interactableObject.InteractableObjectUnsnappedFromDropZone += OnPlugUnsnapped;
        }
        else
        {
            interactableObject.InteractableObjectUnsnappedFromDropZone -= OnPlugUnsnapped;
        }
    }

    protected virtual void OnPlugGrabbed(object sender, InteractableObjectEventArgs e)
    {
        if (PlugGrabbed != null)
        {
            PlugGrabbed(new PlugEventArgs { sender = this });
        }

        // Keep the plug facing down
        faceDown = true;

    }

    protected virtual void OnPlugUngrabbed(object sender, InteractableObjectEventArgs e)
    {
        var io = (VRTK_InteractableObject)sender;

        // if the plug was dropped in an invalid location, return it to the default position
        if (!io.IsHoveredOverSnapDropZone() && !io.IsInSnapDropZone())
        {
            ReturnToDefault();
        }

        faceDown = false;

        if (PlugUngrabbed != null)
        {
            PlugUngrabbed(new PlugEventArgs { sender = this });
        }
    }

    protected virtual void OnPlugSnapped(object sender, InteractableObjectEventArgs e)
    {
        var io = (VRTK_InteractableObject)sender;

        ConnectToPlug(io.GetStoredSnapDropZone().gameObject);
    }

    protected virtual void OnPlugUnsnapped(object sender, InteractableObjectEventArgs e)
    {
        var io = (VRTK_InteractableObject)sender;

        DisconnectFromPlug();
    }

    protected IEnumerator Tween(GameObject obj, Vector3 endPosition, Quaternion endRotation, float duration)
    {
        float elapsedTime = 0f;

        Transform ioTransform = obj.transform;

        Vector3 startPosition = ioTransform.position;
        Quaternion startRotation = ioTransform.rotation;
        //Vector3 startScale = ioTransform.localScale;

        while (elapsedTime <= duration)
        {
            elapsedTime += Time.deltaTime;

            ioTransform.position = Vector3.Lerp(startPosition, endPosition, (elapsedTime / duration));
            ioTransform.rotation = Quaternion.Lerp(startRotation, endRotation, (elapsedTime / duration));
            //ioTransform.localScale = Vector3.Lerp(startScale, endScale, (elapsedTime / duration));

            yield return null;
        }

        //Force all to the last setting in case anything has moved during the transition
        ioTransform.position = endPosition;
        ioTransform.rotation = endRotation;
        //ioTransform.localScale = endScale;
    }

    protected IEnumerator TweenLocal(GameObject obj, Vector3 endPosition, Quaternion endRotation, float duration)
    {
        float elapsedTime = 0f;

        Transform ioTransform = obj.transform;

        Vector3 startPosition = ioTransform.localPosition;
        Quaternion startRotation = ioTransform.localRotation;
        //Vector3 startScale = ioTransform.localScale;

        while (elapsedTime <= duration)
        {
            elapsedTime += Time.deltaTime;

            ioTransform.localPosition = Vector3.Lerp(startPosition, endPosition, (elapsedTime / duration));
            ioTransform.localRotation = Quaternion.Lerp(startRotation, endRotation, (elapsedTime / duration));
            //ioTransform.localScale = Vector3.Lerp(startScale, endScale, (elapsedTime / duration));

            yield return null;
        }

        //Force all to the last setting in case anything has moved during the transition
        ioTransform.localPosition = endPosition;
        ioTransform.localRotation = endRotation;
        //ioTransform.localScale = endScale;
    }
}
