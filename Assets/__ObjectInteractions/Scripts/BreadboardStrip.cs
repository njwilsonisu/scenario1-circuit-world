﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreadboardStrip : MonoBehaviour
{
    // A strip is a Row or Column on a breadboard where all inputs are connected. 

    public GameObject TerminalPlugPrefab;

    public List<GameObject> initialTerminals = new List<GameObject>();

    // each strip contains a set number of input plugs where wires can be plugged into
    public List<Plug_Terminal> terminals = new List<Plug_Terminal>();

    public List<Plug_Base> connectedPlugs = new List<Plug_Base>();

    protected virtual void Awake()
    {
        // copy all of the components from the prefab to each terminal
        foreach(Component component in TerminalPlugPrefab.GetComponents<Component>())
        {
            if (!(component is Plug_Terminal) && !(component is Transform))
            {
                foreach(GameObject g in initialTerminals)
                {
                    g.AddComponent<Component>(component);
                }
            }
        }
    }

    protected virtual void OnEnable()
    {
        foreach( Plug_Terminal t in terminals)
        {
            if (t.isConnected && t.connectedPlug != null)
            {
                Plug_Base p = t.connectedPlug;

                ConnectPlug(p, true);
            }
        }

        ListenForConnectedToTerminal(true);
        ListenForDisconnectedFromTerminal(true);
    }

    protected virtual void OnDisable()
    {
        ListenForConnectedToTerminal(false);
        ListenForDisconnectedFromTerminal(false);
    }

    protected virtual void ConnectPlug(Plug_Base p, bool connect)
    {
        if (!connect)
        {
            if (connectedPlugs.Contains(p))
            {
                connectedPlugs.Remove(p);
            }
        }

        // go through all of the connected plugs...
        foreach (Plug_Base plug in connectedPlugs)
        {
            if (connect)
            {
                // add the new plug's lead to their connected leads
                plug.lead.AddConnectedLead(p.lead);

                // add their lead to the newPlugs connected leads
                p.lead.AddConnectedLead(plug.lead);
            }
            else
            {
                // remove the plug's lead from their connected leads
                plug.lead.RemoveConnectedLead(p.lead);

                // remove their lead from the plug's connected leads
                p.lead.RemoveConnectedLead(plug.lead);
            }
        }

        if (connect)
        {
            if (!connectedPlugs.Contains(p))
            {
                // add the newplug to the list
                connectedPlugs.Add(p);
            }
        }
    }

    public virtual void ListenForConnectedToTerminal(bool listen)
    {
        foreach(Plug_Terminal p in terminals)
        {
            if (p != null)
            {
                if (listen)
                {
                    p.PlugConnected += OnConnectedToTerminal;
                }
                else
                {
                    p.PlugConnected -= OnConnectedToTerminal;
                }
            }
        }
    }

    public virtual void ListenForDisconnectedFromTerminal(bool listen)
    {
        foreach (Plug_Terminal p in terminals)
        {
            if (p != null)
            {
                if (listen)
                {
                    p.PlugDisconnected += OnDisconnectedFromTerminal;
                }
                else
                {
                    p.PlugDisconnected -= OnDisconnectedFromTerminal;
                }
            }
        }
    }

    public virtual void OnConnectedToTerminal(PlugEventArgs e)
    {
        // something was plugged into one of the terminal plugs

        // get the plug that was connected
        Plug_Base p = e.connection;

        // connect the plug
        ConnectPlug(p, true);
    }

    public virtual void OnDisconnectedFromTerminal(PlugEventArgs e)
    {
        // something was unplugged from one of the terminal plugs

        // get the plug that was disconnected
        Plug_Base p = e.connection;

        // disconnect the plug
        ConnectPlug(p, false);
    }
}
