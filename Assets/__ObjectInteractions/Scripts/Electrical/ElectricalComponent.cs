﻿using SharpCircuit;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public struct ElectricalComponentEventArgs
{
    public ElectricalComponent sender;
    // OTHER STUFF
    // - The property that is changed
    // - The new value of the property
}

public delegate void ElectricalComponentEventHandler(ElectricalComponentEventArgs e);

[ExecuteInEditMode]
public abstract class ElectricalComponent : MonoBehaviour
{
    public bool isConnected = false;

    protected bool needsUpdating = false;

    private CircuitElement _circuitElement;
    private List<ElectricalLead> _leads;
    [SerializeField]
    // Each electrical component will use plugs to represent their leads
    private Plug_Base[] _plugs;

    public event ElectricalComponentEventHandler ComponentConnected;
    public event ElectricalComponentEventHandler ComponentDisconnected;

    // -------------------------------------------------------------------------------------------
    //public event ElectricalComponentEventHandler ComponentNeedsUpdating;
    // -------------------------------------------------------------------------------------------

    public CircuitElement circuitElement
    {
        get
        {
            if (_circuitElement == null)
            {
                _circuitElement = GetCircuitElement();
            }
            return _circuitElement;
        }
    }

    // Any component must use this class to return an "CircuitElement" of a specified type
    public abstract CircuitElement GetCircuitElement();

    // Each component will use this function to indicate if any critical changes have happened to it
    public virtual bool NeedsUpdating()
    {
        bool temp = needsUpdating;

        if (needsUpdating)
        {
            needsUpdating = false;
        }

        return temp;
    }

    

    public List<ElectricalLead> leads
    {
        get
        {
            if (_leads == null || _leads.Count < 1)
            {
                _leads = RegenerateLeads();
            }
            return _leads;
        }
    }

    public Plug_Base[] plugs
    {
        get
        {
            if (_plugs == null || _plugs.Length < 1)
            {
                _plugs = RegeneratedPlugs();
            }
            return _plugs;
        }
    }

    protected virtual List<ElectricalLead> RegenerateLeads()
    {
        Type t = circuitElement.GetType();

        List<ElectricalLead> ls = new List<ElectricalLead>();

        // get all of the leads from the element
        foreach (PropertyInfo p in t.GetProperties())
        {
            if (p.PropertyType == typeof(Circuit.Lead))
            {
                ElectricalLead l = new ElectricalLead(this, p.Name, ls.Count, p.GetHashCode());

                ls.Add(l);
            }
        }

        return ls;
    }

    private Plug_Base[] RegeneratedPlugs()
    {
        // create array
        Plug_Base[] t = new Plug_Base[NumberOfLeads()];

        // keep any previous plugs 
        for (int i = 0; i < t.Length; i++)
        {
            if (_plugs != null && i < _plugs.Length && _plugs[i] != null)
            {
                t[i] = _plugs[i];
            }

            // -----------------------------------------------------------------------------------
            // Get the plugs in the children whose...
            // - name matches the name of a lead
            // - owner is this object
            // -----------------------------------------------------------------------------------

            // make sure they have the correct lead reference
            if (t[i] != null)
            {
                t[i].lead = leads[i];
                leads[i].plug = t[i];
            }

        }

        return t;
    }

    /// <summary>
    /// gets a plug at the given index, but as the specified plug type
    /// </summary>
    /// <typeparam name="T"></typeparam> the plug type
    /// <param name="plug"></param> the plug index
    /// <returns></returns>
    public virtual T GetPlug<T>(int plug) where T : Plug_Base
    {
        T p;
        if (plugs[plug] is T)
        {
            p = plugs[plug] as T;
        }
        else
        {
            p = null;
        }

        return p;
    }

    /// <summary>
    /// Returns an array of the plugs with a specific type - if the plugs 
    /// </summary>
    /// <typeparam name="T"></typeparam> the specific plug type
    /// <returns></returns>
    public virtual T[] GetPlugs<T>() where T : Plug_Base
    {
        T[] n = new T[plugs.Length];

        for (int i = 0; i < plugs.Length; i++)
        {
            if (plugs[i] is T)
            {
                n[i] = plugs[i] as T;
            }
            else
            {
                return null;
            }
        }

        return n;
    }

    public virtual void ResetLeadReferences()
    {
        // go through all of the leads...
        for (int i = 0; i < NumberOfLeads(); i++)
        {
            // get the corresponding plug...
            if (plugs[i] != null)
            {
                // set the lead reference
                plugs[i].lead = leads[i];
                // set the plug reference
                leads[i].plug = plugs[i];

                // if the plug is connected...
                if (plugs[i].isConnected)
                {
                    // make sure the leads are connected
                    plugs[i].lead.AddConnectedLead(plugs[i].connectedPlug.lead);
                }
            }
        }
    }

    public virtual int NumberOfLeads()
    {
        return circuitElement.getLeadCount();
    }

    public virtual ElectricalLead GetPlugLead(Plug_Base plug)
    {
        if (plug != null)
        {
            if (plugs.Contains(plug))
            {
                int index = Array.IndexOf(plugs, plug);
                return leads[index];
            }
        }
        return null;
    }

    protected virtual void Awake()
    {
        ResetLeadReferences();
    }

    protected virtual void OnEnable()
    {
        foreach (Plug_Base plug in plugs)
        {
            if (plug != null)
            {
                plug.ListenForPlugConnected(OnPlugConnected, true);
                plug.ListenForPlugDisconnected(OnPlugDisconnected, true);
            }
        }

        // if it was connected but its not anymore
        if (isConnected && !AllPlugsConnected())
        {
            OnComponentDisconnected();
        }
        // if it wasnt connect but now it is
        else if (!isConnected && AllPlugsConnected())
        {
            OnComponentConnected();
        }
    }

    protected virtual void OnDisable()
    {
        foreach (Plug_Base plug in plugs)
        {
            if (plug != null)
            {
                plug.ListenForPlugConnected(OnPlugConnected, false);
                plug.ListenForPlugDisconnected(OnPlugDisconnected, false);
            }
        }
    }

    public virtual void OnPlugConnected(PlugEventArgs e)
    {
        // check if all plugs are plugged in
        if (AllPlugsConnected())
        {
            OnComponentConnected();
        }
    }

    public virtual void OnPlugDisconnected(PlugEventArgs e)
    {
        // if this component was previously connected...
        if (isConnected)
        {
            OnComponentDisconnected();
        }
    }

    protected virtual bool AllPlugsConnected()
    {
        // go through all of the plugs...
        foreach (Plug_Base plug in plugs)
        {
            // if any plug is not connected...
            if (plug == null || !plug.isConnected ) //|| !plug.lead.IsConnected()
            {
                return false;
            }
        }

        return true;
    }

    protected virtual void OnComponentConnected()
    {
        isConnected = true;
        if (ComponentConnected != null)
        {
            ComponentConnected(new ElectricalComponentEventArgs { sender = this });
        }
    }

    protected virtual void OnComponentDisconnected()
    {
        isConnected = false;
        if (ComponentDisconnected != null)
        {
            ComponentDisconnected(new ElectricalComponentEventArgs { sender = this });
        }
    }

    public virtual void ListenForComponentConnected(ElectricalComponentEventHandler e, bool listen)
    {
        if (listen)
        {
            ComponentConnected += e;
        }
        else
        {
            ComponentConnected -= e;
        }
    }

    public virtual void ListenForComponentDisconnected(ElectricalComponentEventHandler e, bool listen)
    {
        if (listen)
        {
            ComponentDisconnected += e;
        }
        else
        {
            ComponentDisconnected -= e;
        }
    }

    //public virtual void ListenForPlugDisconnected(PlugEventHandler p, bool listen)
    //{
    //    if (listen)
    //    {
    //        PlugDisconnected += p;
    //    }
    //    else
    //    {
    //        PlugDisconnected -= p;
    //    }
    //}

}
