﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class ElectricalLead : IEquatable<ElectricalLead>
{

    // This class is used by the electrical components as a reference to the leads so the circuit structure can be generated

    public Plug_Base plug;

    /// <summary>
    /// the component that owns this lead
    /// </summary>
    public ElectricalComponent owner;

    /// <summary>
    /// the name of this lead
    /// </summary>
    public string name;

    /// <summary>
    /// the idex of the owners leads that this lead refers to
    /// </summary>
    public int index;

    public int hash;

    public ElectricalLead(ElectricalComponent owner, string name, int index, int hash)
    {
        this.owner = owner;
        this.name = name;
        this.index = index;
        this.hash = hash;
        //Debug.LogError(owner + " : " + name + " : " + index);
        connectedLeads = new List<ElectricalLead>();
    }

    // a list of leads that are connected to this lead
    public List<ElectricalLead> connectedLeads;

    public bool IsConnected()
    {
        // if there are connected leads...
        if (connectedLeads.Count > 0)
        {
            // and at least one of them is not null...
            foreach (ElectricalLead plug in connectedLeads)
            {
                if (!(plug is null))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public virtual bool IsConnectedToLead(ElectricalLead lead)
    {
        if (lead is null)
        {
            return false;
        }

        return connectedLeads.Contains(lead);
    }

    public virtual void AddConnectedLead(ElectricalLead lead)
    {
        if (lead is null)
        {
            return;
        }

        if (!IsConnectedToLead(lead))
        {
            //Debug.LogError("Connecting " + this.name + " to " + lead.name);
            connectedLeads.Add(lead);
        }
    }

    public virtual void RemoveConnectedLead(ElectricalLead lead)
    {
        if (lead is null)
        {
            return;
        }

        if (IsConnectedToLead(lead))
        {
            //Debug.LogError("Disconnecting " + this.name + " from " + lead.name);
            connectedLeads.Remove(lead);
        }
    }

    public bool Equals(ElectricalLead other)
    {
        if (other == null || !this.GetType().Equals(other.GetType()))
        {
            return false;
        }

        return this.owner == other.owner && this.name == other.name;
    }

    public override int GetHashCode()
    {
        return hash;
    }

    public override string ToString()
    {
        return owner.ToString() + "." + name;
    }
}
