﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CircuitConnection : IEquatable<CircuitConnection>
{
    public ElectricalComponent component1;
    public ElectricalLead lead1;
    public ElectricalComponent component2;
    public ElectricalLead lead2;

    public CircuitConnection(ElectricalLead lead1, ElectricalLead lead2)
    {
        component1 = lead1.owner;
        this.lead1 = lead1;
        component2 = lead2.owner;
        this.lead2 = lead2;
    }

    public bool Equals(CircuitConnection other)
    {
        if (other == null)
        {
            return false;
        }

        var t1 = (this.component1 == other.component1 && this.lead1 == other.lead1) && (this.component2 == other.component2 && this.lead2 == other.lead2);
        var t2 = (this.component1 == other.component2 && this.lead1 == other.lead2) && (this.component2 == other.component1 && this.lead2 == other.lead1);

        return t1 || t2;
    }

    public override int GetHashCode()
    {
        return component1.GetHashCode() + lead1.GetHashCode() + component2.GetHashCode() + lead2.GetHashCode();
    }
}
