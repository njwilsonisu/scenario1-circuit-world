﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plug_Component : Plug_Base
{
    // A ComponentPlug is used to connect one end of a ComponentWire to a Component so the component can 
    //  send/receive info using the wire

    public override bool IsStationary()
    {
        return true;
    }

}
